/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lawncaresystem;

import com.jfoenix.controls.JFXDecorator;
import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.prefs.Preferences;
import lawncaresystem.Database.DBConnection;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import lawncaresystem.Database.DBHelper;
import lawncaresystem.Util.LogUtil;
import lawncaresystem.Util.PdfUtil;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author Jonathon
 * @author Thomas Avery
 */
public class LawnCareSystem extends Application {
    
    public static LogUtil Log = LogUtil.getLogger();
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("gui/Gui.fxml"));
        // TODO: need to fix this decorator / don't want a black border.
        stage.setTitle("Now and Later Lawns");
            String imgPath = ( System.getProperty("os.name").toLowerCase().contains("win") )
                        ? System.getProperty("user.dir")+"\\src\\resources\\images\\Grass.png"
                        : System.getProperty("user.dir")+"/src/resources/images/Grass.png";
        File f = new File( imgPath );
        
        
        stage.getIcons().add(new Image(f.toURI().toString()));
        JFXDecorator decorator = new JFXDecorator(stage, root);
        decorator.setCustomMaximize(true);
        Scene scene = new Scene(decorator);   
        scene.getStylesheets().add(LawnCareSystem.class.getResource("/resources/css/style.css").toExternalForm());
        stage.setScene(scene);
       // stage.setMaximized(true);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        LogUtil.setSystemOutToggle(true);
        Log.i("LawnCareSystem#main", "Starting application");
        Connection conn = DBConnection.getDBConnection();
        DBConnection.startDB();
        
        DBConnection.insertIntoTable("Gray", "Jonathon", "6623477807", "jongray93@gmail.com", "2000 West B Street", "Russellville", "Arkansas", 72801, "testing");
        DBConnection.insertIntoTable("Gray", "Jonathon", "6623477807", "jongray93@gmail.com", "2000 West B Street", "Russellville", "Arkansas", 72801, "NULL");
        try {
           //CustomerLocations
           conn.createStatement().execute("INSERT INTO CustomerLocations(Customer_ID, Address, SquareFt, GrassType, Note)VALUES("
                    + "(SELECT Customer_ID FROM Customers WHERE PhoneNumber = '6623477807' ORDER BY Customer_ID DESC LIMIT 1), '2000 West B Street', 1200, 'Bermuda', 'Nothing to say' )");
           //Jobs
           conn.createStatement().execute("INSERT INTO Jobs(Customer_ID, Address, Date)VALUES("
                    + "(SELECT Customer_ID FROM Customers WHERE PhoneNumber = '6623477807' ORDER BY Customer_ID DESC LIMIT 1), "
                    + "'2000 West B Street',"
                    + "'2017-03-01')");
           //Services
           //conn.createStatement().execute("INSERT INTO Services(ServiceType, Price)VALUES('Mowing', 20.00)");
           //conn.createStatement().execute("INSERT INTO Services(ServiceType, Price)VALUES('Spraying', 15.00)");
           //conn.createStatement().execute("INSERT INTO Services(ServiceType, Price)VALUES('Lights', 40.00)");
           
           //CustomerServices
           conn.createStatement().execute("INSERT INTO CustomerServices(Customer_ID, Address, Service_ID, Price, Delay, NextDate)VALUES("
                    + "(SELECT Customer_ID FROM Customers WHERE PhoneNumber = '6623477807' ORDER BY Customer_ID DESC LIMIT 1),"
                    + "'2000 West B Street', (SELECT Service_ID FROM Services WHERE ServiceType = 'Mowing' ORDER BY Service_ID DESC LIMIT 1), 33.25, 4, '2017-03-08')");
           //JobServices
           conn.createStatement().execute("INSERT INTO JobServices(Job_ID, Service_ID, Price)VALUES("
                    + "(SELECT Job_ID FROM Jobs ORDER BY Job_ID DESC LIMIT 1), (SELECT Service_ID FROM Services ORDER BY Service_ID DESC LIMIT 1), 33.50)");
           //Invoices
           conn.createStatement().execute("INSERT INTO Invoices(Job_ID, Subtotal, TaxPercent, Total, DateSent, DateDue, DatePaid)VALUES("
                    + "(SELECT Job_ID FROM Jobs ORDER BY Job_ID DESC LIMIT 1), 75.34, 9.5, 82.21,"
                    + "'2017-03-01',"
                    + "'2017-03-01',"
                    + "NULL)");
        } catch (SQLException ex) {
            java.util.logging.Logger.getLogger(LawnCareSystem.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //db.insertIntoTable("Jonathon Gray", "2000 West B Street");
        //db.insertIntoTable("Dr. Middleton", "113 Foxwood Village Dr");
        //db.insertIntoTable("Donald Drumph", "1091 East Aspen Lane");
        DBConnection.printTable("Customers");
        DBConnection.printTable("CustomerLocations");
        DBConnection.printTable("Jobs");
        DBConnection.printTable("Services");
        DBConnection.printTable("CustomerServices");
        DBConnection.printTable("JobServices");
        DBConnection.printTable("Invoices");
        //PdfHelper.createPDF();
        //PdfUtil.fillInvoice(null,null,null);
        
        launch(args);
        
    }
    
}
