/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lawncaresystem.Database;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;



/*
     _________________ 
    |Customers        | 
    |-----------------|  
    | *Customer_ID    |  
    | LastName        |  
    | FirstName       | 
    | PhoneNumber     | 
    | Email           |
    | Street          | 
    | City            |      
    | State           |                        
    | ZipCode         | 
    | Note            |
     ----------------- 
     _________________
    |Jobs             |
    |-----------------|
    | *Job_ID         |
    | Customer_ID     |
    | Address         |
    | Date            |
     -----------------
     _________________    
    |Services         |                  
    |-----------------| 
    | *Service_ID     |
    | ServiceType     |
    | Price           | <---- The default service price
    | Active          |
     ----------------- 
     _________________
    |CustomerLocations|
    |-----------------|
    | *Customer_ID    |
    | *Address        |
    | SquareFt        |
    | GrassType       |
    | Note            |
     -----------------
     _________________
    |CustomerServices |
    |-----------------|
    | *Customer_ID    |
    | *Address        |
    | *ServiceType    |
    | Price           | <---- The price for a specific customer
    | Delay           |
    | NextDate        |
     -----------------
     _________________
    | JobServices     |
    |-----------------|
    | *Job_ID         |
    | *ServiceType    |
    | Price           | <---- The price for a specific job
     -----------------
     _________________
    |Invoices         |
    |-----------------|
    | *Job_ID         |
    | Subtotal        |
    | TaxPercent      |
    | Total           | 
    | DateSent        |
    | DateDue         |
    | DatePaid        | <---- NULL if unpaid
     -----------------

*/

/**
 * 
 * @author Jonathon
 */
public class DBHelper {
    private static DBHelper singleton = new DBHelper();
    
    // get fields as array, given table name
    public static HashMap<String, String[]> tableFieldsMap;
    
    public static final String TABLE_CUSTOMERS = "Customers";
    public static final String TABLE_CUSTOMERLOCATIONS = "CustomerLocations";
    public static final String TABLE_JOBS = "Jobs";
    public static final String TABLE_SERVICES = "Services";
    public static final String TABLE_JOBSERVICES = "JobServices";
    public static final String TABLE_CUSTOMERSERVICES = "CustomerServices";
    public static final String TABLE_INVOICES = "Invoices";
    
    
    private DBHelper(){
        tableFieldsMap = new HashMap<String, String[]>();
        tableFieldsMap.put(TABLE_CUSTOMERS, new String[]{
            "Customer_ID", "LastName", "FirstName", "PhoneNumber", "Email", "Street", 
            "City", "State", "ZipCode", "Note"});
        tableFieldsMap.put(TABLE_SERVICES, new String[]{
            "Service_ID", "ServiceType", "Price", "Active"});
        tableFieldsMap.put(TABLE_CUSTOMERLOCATIONS, new String[]{
            "Customer_ID", "Address", "SquareFt", "GrassType", "Note"});
        tableFieldsMap.put(TABLE_JOBS, new String[]{
            "Job_ID", "Customer_ID", "Address", "Date"});
        tableFieldsMap.put(TABLE_JOBSERVICES, new String[]{
            "Job_ID", "Service_ID", "Price"});
        tableFieldsMap.put(TABLE_CUSTOMERSERVICES, new String[]{
            "Customer_ID", "Address", "Service_ID", "Price", "Delay", "NextDate"});
        tableFieldsMap.put(TABLE_INVOICES, new String[]{
            "Job_ID", "Subtotal", "TaxPercent", "Total", "DateSent", "DateDue", "DatePaid"});
    }
    
    public static String getCreateCustomersSQL(){
        StringBuilder sb = new StringBuilder(200);
        sb.append("CREATE TABLE Customers");
        sb.append("(");
        sb.append("Customer_ID INT PRIMARY KEY AUTO_INCREMENT,");
        sb.append("LastName varchar(255) NOT NULL,");
        sb.append("FirstName varchar(255) NOT NULL,");
        sb.append("PhoneNumber varchar(255) NOT NULL,");
        sb.append("Email varchar(255) NOT NULL,");
        sb.append("Street varchar(255) NOT NULL,");
        sb.append("City varchar(255) NOT NULL,");
        sb.append("State varchar(255) NOT NULL,");
        sb.append("ZipCode int NOT NULL,");
        sb.append("Note varchar(255)");
        sb.append(")");
        return sb.toString();
    }
    public static String getCreateServicesSQL(){
        StringBuilder sb = new StringBuilder(200);
        sb.append("CREATE TABLE Services");
        sb.append("(");
        sb.append("Service_ID INT PRIMARY KEY AUTO_INCREMENT,");
        sb.append("ServiceType varchar(255) NOT NULL,");
        sb.append("Price decimal(7,2) NOT NULL,"); 
        sb.append("Active boolean NOT NULL DEFAULT 1");
        
        //sb.append("PRIMARY KEY(Service_ID)");
        sb.append(")");
        return sb.toString();
    }
    public static String getCreateCustomerLocationsSQL(){
        StringBuilder sb = new StringBuilder(200);
        sb.append("CREATE TABLE CustomerLocations");
        sb.append("(");
        sb.append("Customer_ID int NOT NULL,");
        sb.append("Address varchar(255) NOT NULL,");
        sb.append("SquareFt int,");
        sb.append("GrassType varchar(255),");
        sb.append("Note varchar(255),");
        
        sb.append("PRIMARY KEY(Customer_ID, Address),");
        sb.append("FOREIGN KEY(Customer_ID) REFERENCES Customers(Customer_ID)");
        sb.append(")");
        return sb.toString();
    }
    public static String getCreateJobsSQL(){
        StringBuilder sb = new StringBuilder(200);
        sb.append("CREATE TABLE JobS");
        sb.append("(");
        sb.append("Job_ID int PRIMARY KEY AUTO_INCREMENT,");
        sb.append("Customer_ID int NOT NULL,");
        sb.append("Address varchar(255) NOT NULL,");
        sb.append("Date Date NOT NULL,");

        //sb.append("FOREIGN KEY(Address) REFERENCES CustomerLocations(Address),");
        sb.append("FOREIGN KEY(Customer_ID) REFERENCES Customers(Customer_ID)");
        sb.append(")");
        return sb.toString();
    }
    public static String getCreateCustomerServicesSQL(){
        StringBuilder sb = new StringBuilder(200);
        sb.append("CREATE TABLE CustomerServices");
        sb.append("(");
        sb.append("Customer_ID int NOT NULL,");
        sb.append("Address varchar(255) NOT NULL,");
        sb.append("Service_ID int NOT NULL,");
        sb.append("Price decimal(7,2) NOT NULL,");
        sb.append("Delay int NOT NULL,");
        sb.append("NextDate date NOT NULL,");
        
        sb.append("PRIMARY KEY(Customer_ID, Address, Service_ID),");
        sb.append("FOREIGN KEY(Customer_ID) REFERENCES Customers(Customer_ID),");
        //sb.append("FOREIGN KEY(Address) REFERENCES CustomerLocations(Address),");
        sb.append("FOREIGN KEY(Service_ID) REFERENCES Services(Service_ID)");
        sb.append(")");
        return sb.toString();
    }
    public static String getCreateJobServicesSQL(){        
        StringBuilder sb = new StringBuilder(200);
        sb.append("CREATE TABLE JobServices");
        sb.append("(");
        sb.append("Job_ID int NOT NULL,");
        sb.append("Service_ID int NOT NULL,");
        sb.append("Price decimal(7,2) NOT NULL,");
        
        sb.append("PRIMARY KEY(job_ID, Service_ID),");
        sb.append("FOREIGN KEY(Service_ID) REFERENCES Services(Service_ID),");
        sb.append("FOREIGN KEY(Job_ID) REFERENCES Jobs(Job_ID)");
        sb.append(")");
        return sb.toString();
    }
    public static String getCreateInvoicesSQL(){
        StringBuilder sb = new StringBuilder(200);
        sb.append("CREATE TABLE Invoices");
        sb.append("(");
        sb.append("Job_ID int NOT NULL,");
        sb.append("Subtotal decimal(8,2) NOT NULL,");
        sb.append("TaxPercent decimal(4,2) NOT NULL,");
        sb.append("Total decimal(8,2) NOT NULL,");
        sb.append("DateSent date NOT NULL,");
        sb.append("DateDue date NOT NULL,");
        sb.append("DatePaid date,");
        sb.append("PRIMARY KEY(Job_ID),");
        sb.append("FOREIGN KEY(Job_ID) REFERENCES Jobs(Job_ID)");
        sb.append(")");
        return sb.toString();
    }
    public static void insertIntoCustomers(String lastName, String firstName, String phone, String address, String note){
        StringBuilder sb = new StringBuilder(200);
        sb.append("INSERT INTO Customers");
        sb.append("(LastName, FirstName, PhoneNumber, BillingAddress, Note)");
        sb.append("VALUES");
        sb.append("(");
        if(null != lastName) sb.append("'").append(lastName).append("',"); 
        else sb.append("NULL,");        
        if(null != firstName) sb.append("'").append(firstName).append("',"); 
        else sb.append("NULL,");        
        if(null != phone) sb.append("'").append(phone).append("',"); 
        else sb.append("NULL,");        
        if(null != address) sb.append("'").append(address).append("',"); 
        else sb.append("NULL,");        
        if(null != note) sb.append("'").append(note).append("'"); 
        else sb.append("NULL");        
        sb.append(");");
    }
    
    /**
     * 
     *  
     * @return
     */
    public static boolean insertCustomer(String lastName, String firstName, String phoneNumber, String street, String city, String state, String zip, String note){
        
        if(Stream.of(lastName, firstName, phoneNumber, street, city, state, zip).allMatch(x->x!=null)){
            StringBuilder sb = new StringBuilder();
            sb.append("INSERT INTO Customers(LastName, FirstName, PhoneNumber, Street, City, State, ZipCode, Note)VALUES(");
            sb.append("'").append(lastName).append("',");
            sb.append("'").append(firstName).append("',");
            sb.append("'").append(phoneNumber).append("',");
            sb.append("'").append(street).append("',");
            sb.append("'").append(city).append("',");
            sb.append("'").append(state).append("',");
            sb.append("'").append(zip).append("',");
            if(note!=null)
                sb.append("'").append(note).append("')");
            else
                sb.append("NULL)");
            
            try {
                Connection conn = DBConnection.getDBConnection();
                conn.createStatement().execute(sb.toString());
                conn.close();
            } catch (SQLException ex) {
                lawncaresystem.Util.LogUtil.getLogger().e("DBHelper#insertCustomer", "SQLException thrown while inserting customer");
                Logger.getLogger(DBHelper.class.getName()).log(Level.SEVERE, null, ex);
                
            }
            
        }else{
            lawncaresystem.Util.LogUtil.getLogger().e("DBHelper#insertCustomer", "Cannot insert since non-nullable value is null!");
        }
        
        return false;
    }
    public static boolean insertCustomerLocation(){
     
        return false;
    }
    public static boolean insertService(){
     
        return false;
    }
    public static boolean insertJob(){
     
        return false;
    }
    public static boolean insertCustomerService(){
     
        return false;
    }
    public static boolean insertJobService(){
     
        return false;
    }
    public static boolean insertInvoice(){
     
        return false;
    }
    
    // Delete Rows
    
    public static boolean deleteCustomer(){
     
        return false;
    }
    public static boolean deleteCustomerLocation(){
     
        return false;
    }
    public static boolean deleteService(){
     
        return false;
    }
    public static boolean deleteJob(){
     
        return false;
    }
    public static boolean deleteCustomerService(){
     
        return false;
    }
    public static boolean deleteJobService(){
     
        return false;
    }
    public static boolean deleteInvoice(){
     
        return false;
    }
    
}