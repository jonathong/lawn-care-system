/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lawncaresystem.Database;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jonathon
 */
public class DBConnection {
    //private static DBConnection singleton=null;
    //private static final String DB_DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
    //private static final String DB_CONNECTION = "jdbc:derby:lawncaresystem;create=true"; // creates a new DB if there isn't one created. 
    private static final String DB_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DB_CONNECTION = "jdbc:mysql://localhost:3306/lawncaredb";
    private static final String DB_USER = "root";
    private static final String DB_PASSWORD = "root";
    
    public static void startDB(){
        Connection conn = getDBConnection();
        try {
            
            if(conn != null){
                System.out.println("Connected to Database!");
            }
            
            if(!isTableExist(DBHelper.TABLE_CUSTOMERS)){
                createTables();
                System.out.println("Database created");
            }
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static Connection getDBConnection(){
        Connection dbConnection = null;
        try {
            Class.forName(DB_DRIVER);
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
        try {
            dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER,
                    DB_PASSWORD);
            return dbConnection;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return dbConnection;
    }
    public static void createTables(){
        Connection conn = getDBConnection();
        try {
            if(!isTableExist(DBHelper.TABLE_CUSTOMERS));
                conn.createStatement().execute(DBHelper.getCreateCustomersSQL());
            if(!isTableExist(DBHelper.TABLE_CUSTOMERLOCATIONS));
                conn.createStatement().execute(DBHelper.getCreateCustomerLocationsSQL());
            if(!isTableExist(DBHelper.TABLE_JOBS));
                conn.createStatement().execute(DBHelper.getCreateJobsSQL());
            if(!isTableExist(DBHelper.TABLE_SERVICES));
                conn.createStatement().execute(DBHelper.getCreateServicesSQL());
            if(!isTableExist(DBHelper.TABLE_CUSTOMERSERVICES));
                conn.createStatement().execute(DBHelper.getCreateCustomerServicesSQL());
            if(!isTableExist(DBHelper.TABLE_JOBSERVICES));
                conn.createStatement().execute(DBHelper.getCreateJobServicesSQL());
            if(!isTableExist(DBHelper.TABLE_INVOICES));
                conn.createStatement().execute(DBHelper.getCreateInvoicesSQL());
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    public static void insertIntoTable(String lastName, String firstName, String phoneNumber, String email, String street, String city, String state, int zip, String note){
        try {
            StringBuilder sb = new StringBuilder(200);
            sb.append("INSERT INTO Customers(LastName, FirstName, PhoneNumber, Email, Street, City, State, ZipCode, Note)");
            sb.append("VALUES(");
            sb.append("'").append(lastName).append("',");
            sb.append("'").append(firstName).append("',");
            sb.append("'").append(phoneNumber).append("',");
            sb.append("'").append(email).append("',");
            sb.append("'").append(street).append("',");
            sb.append("'").append(city).append("',");
            sb.append("'").append(state).append("',");
            sb.append("").append(zip).append(",");
            if(note!=null)
                sb.append("'").append(note).append("'");
            else
                sb.append("NULL");
            sb.append(")");
            //conn.createStatement().execute(sb.toString());
            Connection conn = getDBConnection();
            conn.createStatement().execute(sb.toString());
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }  
    }
    public static void printTable(String table){
        try {
            Statement statement = getDBConnection().createStatement();
            ResultSet res = statement.executeQuery("Select * FROM "+table);
            System.out.println("TABLE: "+table);
            String[] fields  = DBHelper.tableFieldsMap.get(table);
            int l = fields.length;
            System.out.print(fields[0]);
            for(int i=1; i<l; ++i){
                System.out.print("   -   "+fields[i]);
            }
            System.out.print("\n");
            while(res.next()){
                System.out.print("   ");
                for(int i=0; i<l; i++){
                    if(i+1==l)
                        System.out.print(res.getString(fields[i])+"\n");
                    else
                        System.out.print(res.getString(fields[i])+"   -   ");
                }
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static void printAll(){
        Connection conn = getDBConnection();
        try {
            Statement statement = conn.createStatement();
            ResultSet res = statement.executeQuery("Select * FROM "+DBHelper.TABLE_CUSTOMERS);
            System.out.println("TABLE: Customers:");
            while(res.next()){
                System.out.println("\t"+res.getString("Customer_ID")+" - "+res.getString("LastName")+" - "+res.getString("FirstName")+" - "+res.getString("ZipCode"));
            }
            System.out.println("TABLE: CustomerLocations:");
            res = conn.createStatement().executeQuery("SELECT * FROM CustomerLocations");
            while(res.next()){
                System.out.println("\t"+res.getString("Customer_ID")+" - "+res.getString("Address") +" - "+res.getString("SquareFt")+" - " + res.getString("GrassType") + " - "+res.getString("Note"));
            }
            System.out.println("TABLE: Jobs:");
            res = conn.createStatement().executeQuery("SELECT * FROM Jobs");
            while(res.next()){
                System.out.println("\t"+res.getString("Job_ID")+" - "+res.getString("Customer_ID") +" - "+res.getString("Address")+" - "+res.getString("Date"));
            }
            System.out.println("TABLE: Services:");
            res = conn.createStatement().executeQuery("SELECT * FROM Services");
            
            conn.close();
            
            while(res.next()){
                System.out.println("\t"+res.getString("Service_ID")+" - "+res.getString("ServiceType") +" - "+res.getString("Price"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
public void showTbls(){
        try {
            DatabaseMetaData dbMetaData = getDBConnection().getMetaData();
            //getting catalogs for mysql DB, if it is not working for your DB, try dbMetaData.getSchemas();
            ResultSet catalogs = dbMetaData.getCatalogs();
            while(catalogs.next()){
                String catalogName = catalogs.getString(1);
                //excluding table names from "mysql" schema from mysql DB.
                if(!"mysql".equalsIgnoreCase(catalogName)){
                    ResultSet tables = dbMetaData.getTables(catalogName, null, null, null);
                    while(tables.next()){
                        System.out.println(catalogName + "::"+tables.getString(3));
                    }
                }
            }     } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
}
    
    public static boolean isTableExist(String table) throws SQLException{
        Connection conn = getDBConnection();
        if(conn!=null){
        
            DatabaseMetaData dbmd = conn.getMetaData();
            ResultSet rs = dbmd.getTables(null, null, table.toUpperCase(),null);
            if(rs.next()){
                if(table.toUpperCase().equals(rs.getString("TABLE_NAME").toUpperCase())){
                    System.out.println("Table "+rs.getString("TABLE_NAME")+" already exists !!");
                    return true;
                }
            }
        }
        conn.close();
        System.out.println(table + " does not exist");   
        return false;
    }
}
