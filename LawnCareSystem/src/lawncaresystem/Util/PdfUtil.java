/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lawncaresystem.Util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.util.Pair;
import javax.swing.UIManager;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;


/**
 *
 * @author Jonathon
 */
public class PdfUtil {
    
    public static void fillInvoice(String name, String address, ArrayList<Pair<String, Double>> jobsAndCosts){
        String jrxmlFileName = System.getProperty("user.dir")+"\\src\\resources\\templates\\report1.jrxml";
        String jasperFileName = System.getProperty("user.dir")+"\\src\\resources\\templates\\report1.jasper";
        String pdfFileName = System.getProperty("user.dir")+"\\src\\resources\\report1.pdf";
        String logoFileName = System.getProperty("user.dir")+"\\src\\resources\\images\\logo.jpg";
        
        if(!System.getProperty("os.name").contains("win")){
            System.out.println("os is not windows.");
            jrxmlFileName = jrxmlFileName.replace("\\", "/");
            jasperFileName = jasperFileName.replace("\\", "/");
            pdfFileName = pdfFileName.replace("\\", "/");
            logoFileName = logoFileName.replace("\\", "/");

        }
        System.out.println(jrxmlFileName);
        System.out.println(jasperFileName);
        System.out.println(pdfFileName);
        try{
            // This compilation step can be removed later.
            JasperCompileManager.compileReportToFile(jrxmlFileName, jasperFileName);  
            
            Map hm = new HashMap();
            hm.put("address", "2000 West B street");
            hm.put("name", "Jonathon Gray");
            hm.put("logo", logoFileName);
            List<Map<String, ?>> maps = new ArrayList<Map<String, ?>>();
            maps.add(hm);
            
            JRMapCollectionDataSource datasrc = new JRMapCollectionDataSource(maps);
            JasperPrint jprint = (JasperPrint)JasperFillManager.fillReport(jasperFileName, hm, datasrc);
            JasperExportManager.exportReportToPdfFile(jprint, pdfFileName);
            JasperViewer jv = new JasperViewer(jprint,false);
            
            // Show pdf to user
            //jv.setVisible(true);
            // Print pdf for user
            //JasperPrintManager.printReport( jprint, true);
            
        }catch(Exception e){
            e.printStackTrace();
        } 
    }

}
