/*
 Thomas Avery 
 2-21-17
 System Anaylsis 2
 GMail via TLS using JavaMail API library source from https://java.net/projects/javamail/pages/Home
 Modified code from mkyong https://www.mkyong.com/java/javamail-api-sending-email-via-gmail-smtp-example/

Notice this is now using the actual password for the account 
and Allow less secure apps: ON is turned on in the gmail account 

I suggest asking the user to make a two step authentation for his account via his phone and 
then setting up a Application specfic password bellow as described The turning Allow less secure apps: OFF.
 */
package lawncaresystem.Util;

import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 *
 * @author Jonathon
 * @author Thomas
 */
public class EmailUtil {
    public static void emailCall(String to, String subject, String msg, String attachment) {

        final String username = "nowandlaterlawns@gmail.com";
        final String password = "clockalarm";   // "Application Specific Password Goes here"; //https://support.google.com/accounts/answer/185833?hl=en

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
          new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                }
          });

        try {

                Message message = new MimeMessage(session);
                message.setFrom(new InternetAddress(username));
                message.setRecipients(Message.RecipientType.TO,
                        InternetAddress.parse(to));
                message.setSubject(subject);
                
                //if
                if(attachment != null){
                BodyPart messageBodyPart = new MimeBodyPart();
                
                messageBodyPart.setText(msg);
                
                Multipart multipart = new MimeMultipart();
                
                multipart.addBodyPart(messageBodyPart);
                
                 messageBodyPart = new MimeBodyPart();
                 String filename = attachment;
                 DataSource source = new FileDataSource(filename);
                 messageBodyPart.setDataHandler(new DataHandler(source));
                 messageBodyPart.setFileName(filename);
                 multipart.addBodyPart(messageBodyPart);
                
                
               // message.setText(msg);
               message.setContent(multipart);
               
                Transport.send(message);
                }
                else {
                    message.setText(msg);
                    Transport.send(message);
                }
                
              //  System.out.println("Done");

        } catch (MessagingException e) {
                throw new RuntimeException(e);
        }
    }
}
