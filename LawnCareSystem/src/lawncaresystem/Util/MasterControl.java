/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lawncaresystem.Util;

import java.util.prefs.Preferences;

/**
 *
 * @author jonathongray
 */
public class MasterControl {
    private static MasterControl singleton=null;
    private static Preferences userPreferences = Preferences.userNodeForPackage(MasterControl.class);
    
    private MasterControl(){
    }
    
    public static MasterControl access(){
        if(singleton==null)
            singleton = new MasterControl();
        return singleton;
    }
    
    //getters
    public String getEmail(){
        return userPreferences.get("email", "");
    }
    public String getPassword(){
        return userPreferences.get("password", "");
    }
    public double getTaxPercent(){
        return userPreferences.getDouble("taxPercent", 9.5);
    }
    
    //setters
    public void setEmail(String e){
        userPreferences.put("email", e);
    }
    public void setPassword(String p){
        userPreferences.put("password", p);
    }
    public void setTaxPercent(double t){
        userPreferences.putDouble("taxPercent", t);
    }
    
}
