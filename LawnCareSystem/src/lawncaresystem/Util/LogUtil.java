/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lawncaresystem.Util;

import java.util.ArrayList;

/**
 *
 * @author Jonathon Gray
 * 
 * A simple Logging class that also allows for observers to be updated.
 * 
 * 
 */
public class LogUtil {
    public static final int LEVEL_FATAL = 4;
    public static final int LEVEL_ERROR = 3;
    public static final int LEVEL_WARNING = 2;
    public static final int LEVEL_INFO = 1;
    public static final int LEVEL_DEBUG = 0;
   
    private static LogUtil single;
    
    ArrayList<LogObserver> observers = new ArrayList();
    private static int levelFilter = 0;
    private static boolean systemOutTog = false;

    private LogUtil(){}
    
    public static LogUtil getLogger(){
        if(single == null)
            single = new LogUtil();
        return single;
    }
    public static void setLevelFilter(int level){
        levelFilter = level;
    }
    public static int getLevelFilter(){
        return levelFilter;
    }
    public void register(LogObserver o){
        if(o == null){
            System.out.println("Error registering observer, observer null");
            return;
        }
        if(observers.contains(o)){
            System.out.println("Error regisetering observer, already registered");
        }
        observers.add(o);
    }
    public void unregister(LogObserver o){
        if(o == null){
            System.out.println("Error registering observer, observer null");
            return;
        }
        observers.remove(o);
    }
    public static void setSystemOutToggle(boolean t){
        systemOutTog = t;
    }
    public int observerCount(){
        return observers.size();
    }
    private void notifyObservers(LogMessage l){
        if(systemOutTog)
            System.out.println(l.toString());
        
        for(LogObserver o : observers){
            if(o instanceof LogObserver)
                o.update(l);
        }
    }
    
    public void d(String tag, String message){ debug(tag, message); }
    public void i(String tag, String message){ info(tag, message); }
    public void w(String tag, String message){ warn(tag, message); }
    public void e(String tag, String message){ error(tag, message); }
    public void f(String tag, String message){ fatal(tag, message); }
    
    public void debug(String Tag, String message){
        if(LEVEL_DEBUG >= levelFilter)
            notifyObservers(new LogMessage("DEBUG",message, Tag ));
    }
    public void info(String Tag, String message){
        if(LEVEL_INFO >= levelFilter)
            notifyObservers(new LogMessage("INFO",message, Tag ));
    }
    public void warn(String Tag, String message){
        if(LEVEL_WARNING >= levelFilter)
            notifyObservers(new LogMessage("WARNING",message, Tag ));
    }
    public void error(String Tag, String message){
        if(LEVEL_ERROR >= levelFilter)
            notifyObservers(new LogMessage("ERROR",message, Tag ));
    }
    public void fatal(String Tag, String message){
        if(LEVEL_FATAL >= levelFilter)
            notifyObservers(new LogMessage("FATAL", message, Tag ));
    }
}

class LogMessage {
    String level;
    String message;
    String tag;
    LogMessage(){
        this("", "");
    }
    LogMessage(String l, String m){
        this(l,m,"");
    }
    LogMessage(String l, String m, String t){
        level = l;
        message = m;
        tag = t;
    }
    public String getLevel() {
        return level;
    }

    public String getMessage() {
        return message;
    }
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(level);
        sb.append("]");
        sb.append(tag);
        sb.append(" - ");
        sb.append(message);
        sb.append("\n");
        return sb.toString();
    }
}

interface LogObserver {
    void update(LogMessage l);
}

