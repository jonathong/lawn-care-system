/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lawncaresystem.Util;

import java.util.ArrayList;
import java.util.logging.Level;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/*
Example of json:

{  
   "services":[  
      {  
         "price":23.0,
         "type":"mow"
      },
      {  
         "price":19.99,
         "type":"spray"
      },
      {  
         "price":14.99,
         "type":"trim"
      },
      {  
         "price":85.49,
         "type":"light"
      }
   ]
}

*/


/**
 *
 * @author Jonathon
 */

public class JsonUtil {
    
    public String serviceAndPriceToJSON(ArrayList<String> servicesArr, ArrayList<Float> pricesArr){
        JSONObject json = new JSONObject();
        JSONArray array = new JSONArray();
        
        if(servicesArr.size()!=pricesArr.size()){
            LogUtil.getLogger().e("JsonUtil#serviceAndPriceToJSON", "array lengths are different!  Cannot store all services and prices.");
        }
            
        int l = servicesArr.size();
        for(int i=0; i<l; ++i){
            JSONObject o = new JSONObject();
            o.put("Type", servicesArr.get(i));
            o.put("Price", pricesArr.get(i));
            array.add(o);
        }
        json.put("services", array);
        
        return json.toJSONString();
    }
    public JSONArray getServicesJSONArray(String json){
        JSONArray array = new JSONArray();
        JSONParser parser = new JSONParser();
        
        try {
            JSONObject o = (JSONObject)parser.parse(json);
            return (JSONArray)o.get("services");
            
        } catch (ParseException ex) {
            LogUtil.getLogger().e("JsonUtil#getServicesJsonArray", "Exception while parsing json string");
            ex.printStackTrace();
        }
        
        return null;
    }
}
