/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lawncaresystem.gui.Customers;

import lawncaresystem.gui.Billing.*;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Jonathon
 * 
 */

public class CustomerRow {
    private final SimpleIntegerProperty customerID;
    private final SimpleStringProperty lastName;
    private final SimpleStringProperty firstName;
    private final SimpleStringProperty phoneNumber;
    private final SimpleStringProperty email;
    private final SimpleStringProperty street;
    private final SimpleStringProperty city;
    private final SimpleStringProperty state;
    private final SimpleStringProperty zipCode;
    private final SimpleStringProperty note;
    
    
    
    public CustomerRow(String cID, String lName, String fName, String phone, String email,
            String street, String city, String state, String zip, String note){
        
        this.customerID = new SimpleIntegerProperty(Integer.parseInt(cID));
        this.lastName = new SimpleStringProperty(lName);
        this.firstName = new SimpleStringProperty(fName);
        this.phoneNumber = new SimpleStringProperty(phone);
        this.email = new SimpleStringProperty(email);
        this.street = new SimpleStringProperty(street);
        this.city = new SimpleStringProperty(city);
        this.state = new SimpleStringProperty(state);
        this.zipCode = new SimpleStringProperty(zip);
        this.note = new SimpleStringProperty(note);
    }

    public Integer getCustomerID() {
        return customerID.get();
    }

    public String getLastName() {
        return lastName.get();
    }

    public String getFirstName() {
        return firstName.get();
    }

    public String getPhoneNumber() {
        return phoneNumber.get();
    }
    
    public String getEmail(){
        return email.get();
    }

    public String getStreet() {
        return street.get();
    }

    public String getCity() {
        return city.get();
    }

    public String getState() {
        return state.get();
    }

    public String getZipCode() {
        return zipCode.get();
    }
    
    public String getNote(){
        if(note.get()==null)
            return "";
        else
            return note.get();
    }
    
    public void setCustomerID(String id){
        customerID.set(Integer.parseInt(id));
    }
    
    public void setLastName(String n){
        lastName.set(n);
    }
    
    public void setFirstName(String n){
        firstName.set(n);
    }
    
    public void setPhoneNumber(String ph){
        phoneNumber.set(ph);
    }
    
    public void setEmail(String em){
        email.set(em);
    }
    
    public void setStreet(String s){
        street.set(s);
    }
    
    public void setCity(String c){
        city.set(c);
    }
    
    public void setState(String st){
        state.set(st);
    }
    
    public void setZipCode(String z){
        zipCode.set(z);
    }
    public void setNote(String n){
        note.set(n);
    }

    
}
