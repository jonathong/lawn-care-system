/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lawncaresystem.gui.Customers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDecorator;
import com.jfoenix.controls.JFXSnackbar;
import com.jfoenix.controls.JFXSnackbar.SnackbarEvent;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.stage.FileChooser;
import javax.management.Notification;
import lawncaresystem.Database.DBConnection;
/**
 * FXML Controller class
 *
 * @author Thomas
 */
public class EmailCustomerController implements Initializable {
       @FXML
    public JFXTextField toTextField = new JFXTextField();
        @FXML
    public JFXTextField subjectTextField= new JFXTextField();
         @FXML
    public JFXTextArea messageTextArea = new JFXTextArea();
    @FXML
    public JFXTextField attachmentTextField= new JFXTextField();
 
     public String attachment = null;
         /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    public void setToField(String email){
        toTextField.setText(email);
    }
    public void sendButtonClicked(){
        String to = toTextField.getText();
        String subject = subjectTextField.getText();
        String message = messageTextArea.getText();
        lawncaresystem.Util.EmailUtil.emailCall(to,subject,message,attachment);
    }
    
    public void discardButtonClicked(){
        
    }
     public void attachButtonClicked(){
      FileChooser fc = new FileChooser();
      File selectedFile = fc.showOpenDialog(null);
      if(selectedFile != null){
          attachment = selectedFile.getPath();
          
      }
      else{
         System.out.println("No file selected!");
     }
    }
}
