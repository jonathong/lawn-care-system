/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lawncaresystem.gui.Customers;

import java.time.LocalDate;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author jonathongray
 */
public class CustomerServiceRow {
    private final SimpleIntegerProperty serviceID;
    private final SimpleStringProperty name;
    private final SimpleStringProperty price;
    private final SimpleStringProperty interval;
    private final SimpleObjectProperty<LocalDate> nextDate;
    
    public CustomerServiceRow(String id, String n, String p, String i, LocalDate d){
        
        this.serviceID = new SimpleIntegerProperty(Integer.parseInt(id));
        this.name = new SimpleStringProperty(n);
        this.price = new SimpleStringProperty(p);
        this.interval = new SimpleStringProperty(i);
        this.nextDate = new SimpleObjectProperty<LocalDate>(d);

    }

    public Integer getServiceID() {
        return serviceID.get();
    }

    public String getName() {
        return name.get();
    }

    public String getPrice() {
        return price.get();
    }

    public String getInterval(){
        return interval.get();
    }
    
    public LocalDate getNextDate(){
        return nextDate.get();
    }
    
    public void setServiceID(String id){
        serviceID.set(Integer.parseInt(id));
    }
    
    public void setName(String n){
        name.set(n);
    }
    
    public void setPrice(String p){
        price.set(p);
    }
    
    public void setInterval(String i){
        interval.set(i);
    }
    
    public void setNextDate(LocalDate d){
        nextDate.set(d);
    }
}
