/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lawncaresystem.gui.Customers;

import com.jfoenix.controls.JFXDecorator;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import lawncaresystem.Database.DBConnection;
import lawncaresystem.LawnCareSystem;
import lawncaresystem.Util.LogUtil;
import lawncaresystem.gui.Billing.BillingController;
import lawncaresystem.gui.Billing.BillingRow;
import sun.awt.image.ByteBandedRaster;

/**
 * FXML Controller class
 *
 * @author Jonathon
 */
public class CustomersController implements Initializable {

    @FXML
    public TableView tableView;

    @FXML
    private JFXTextField filterField;
    
    private TableColumn col_customerID = new TableColumn("Customer_ID");
    private TableColumn col_lastName = new TableColumn("LastName");
    private TableColumn col_firstName = new TableColumn("FirstName");
    private TableColumn col_phoneNumber = new TableColumn("PhoneNumber");
    private TableColumn col_email = new TableColumn("Email");
    private TableColumn col_street = new TableColumn("Street");
    private TableColumn col_city = new TableColumn("City");
    private TableColumn col_state = new TableColumn("State");
    private TableColumn col_zipCode = new TableColumn("ZipCode");
    private TableColumn col_note = new TableColumn("Note");

    
    private final ObservableList<CustomerRow> data = FXCollections.observableArrayList();
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        // First, initialize the table and columns
        tableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        
        initializeColumns();
        populateDataset();
        setUpSearchFunction();
    }
    private void setUpSearchFunction(){
                // 1. Wrap the ObservableList in a FilteredList (initially display all data).
        FilteredList<CustomerRow> filteredData = new FilteredList<>(data, e -> true);

        // 2. Set the filter Predicate whenever the filter changes.
        filterField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(customer -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();

                if (customer.getFirstName().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches first name.
                } else if (customer.getLastName().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                } else if ((customer.getCustomerID()+"").contains(lowerCaseFilter) ){
                    return true;
                } else if(customer.getEmail().toLowerCase().contains(lowerCaseFilter)){
                    return true;
                } else if(customer.getStreet().toLowerCase().contains(lowerCaseFilter)){
                    return true;
                } else if(customer.getPhoneNumber().toLowerCase().contains(lowerCaseFilter)){
                    return true;
                }else if(customer.getNote().toLowerCase().contains(lowerCaseFilter)){
                    return true;
                }
                return false; // Does not match.
            });
        });
        // 3. Wrap the FilteredList in a SortedList. 
        SortedList<CustomerRow> sortedData = new SortedList<>(filteredData);
        // 4. Bind the SortedList comparator to the TableView comparator.
        sortedData.comparatorProperty().bind(tableView.comparatorProperty());
        // 5. Add sorted (and filtered) data to the table.
        tableView.setItems(sortedData);
    }
    public void refreshTable(){
        data.clear();
        populateDataset();
        setUpSearchFunction();
    }
    private void populateDataset(){
        
        String query = "SELECT * FROM Customers";

        try{
            Connection conn = DBConnection.getDBConnection();
            Statement statement = conn.createStatement();
            ResultSet res = statement.executeQuery(query);
            while(res.next()){
                data.add(new CustomerRow(
                        res.getString("Customer_ID"),
                        res.getString("LastName"),
                        res.getString("FirstName"),
                        res.getString("PhoneNumber"),
                        res.getString("Email"),
                        res.getString("Street"),
                        res.getString("City"),
                        res.getString("State"),
                        res.getString("ZipCode"),
                        res.getString("Note")
                ));
            }
            res.close();
            conn.close();
        }catch(SQLException sqle){
            LogUtil.getLogger().e("CustomerController#populateTables", "SQL Exception thrown while querying for Customer and Invoice data");
            sqle.printStackTrace();
        }
        
        tableView.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override 
            public void handle(MouseEvent event) {
                if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
                    Node node = ((Node) event.getTarget()).getParent();
                    TableRow row;
                    
                    if (node instanceof TableRow) {
                        row = (TableRow) node;
                    } else {
                        // clicking on text part
                        row = (TableRow) node.getParent();
                    }
                    CustomerRow c = (CustomerRow)row.getItem();
                    customerRowClicked(c);
                    System.out.println(row.getItem());
                }
            }
        });
        
    }
    private void initializeColumns(){
        col_customerID.setMinWidth(25);
        col_customerID.setPrefWidth(40);
        col_customerID.setResizable(true);
        col_customerID.setCellValueFactory(new PropertyValueFactory<CustomerRow, String>("customerID"));
        
        col_lastName.setMinWidth(50);
        col_lastName.setPrefWidth(100);
        col_lastName.setCellValueFactory(new PropertyValueFactory<CustomerRow, String>("lastName"));
        
        col_firstName.setMinWidth(50);
        col_firstName.setPrefWidth(100);
        col_firstName.setCellValueFactory(new PropertyValueFactory<CustomerRow, String>("firstName"));
        
        col_phoneNumber.setMinWidth(50);
        col_phoneNumber.setPrefWidth(80);
        col_phoneNumber.setCellValueFactory(new PropertyValueFactory<CustomerRow, String>("phoneNumber"));
        
        col_email.setMinWidth(50);
        col_email.setPrefWidth(80);
        col_email.setCellValueFactory(new PropertyValueFactory<CustomerRow, String>("email"));
        
        col_street.setMinWidth(50);
        col_street.setPrefWidth(80);
        col_street.setCellValueFactory(new PropertyValueFactory<CustomerRow, String>("street"));
        
        col_city.setMinWidth(50);
        col_city.setPrefWidth(80);
        col_city.setCellValueFactory(new PropertyValueFactory<CustomerRow, String>("city"));
        
        col_state.setMinWidth(50);
        col_state.setPrefWidth(80);
        col_state.setCellValueFactory(new PropertyValueFactory<CustomerRow, String>("state"));
        
        col_zipCode.setMinWidth(50);
        col_zipCode.setPrefWidth(80);
        col_zipCode.setCellValueFactory(new PropertyValueFactory<CustomerRow, String>("zipCode"));
        
        col_note.setMinWidth(80);
        col_note.setPrefWidth(80);
        col_note.setCellValueFactory(new PropertyValueFactory<CustomerRow, String>("note"));
        
        tableView.getColumns().addAll(col_customerID, col_lastName, col_firstName, col_phoneNumber
                ,col_email, col_street, col_city, col_state, col_zipCode, col_note);
        tableView.setTableMenuButtonVisible(true);
        tableView.setColumnResizePolicy(TableView.UNCONSTRAINED_RESIZE_POLICY);
        
    }
    public void deleteCustomerButtonClicked(){
        ObservableList l = tableView.getSelectionModel().getSelectedCells();
        ObservableList<CustomerRow> objects = tableView.getSelectionModel().getSelectedItems();
        
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirm Deletion");
        alert.setHeaderText(null);
        alert.setContentText("Are you sure you want to delete "+l.size()+" customers from your database? "
                + "This action cannot be undone and will remove all invoices and jobs associated with this customer.");
        Optional <ButtonType> action = alert.showAndWait();
        
        if(action.get() == ButtonType.OK){
            for(int i=0; i<l.size();++i){
                int row = ((TablePosition) l.get(i) ).getRow();
                Integer id = (Integer)col_customerID.getCellData(row);
                try {
                    Connection conn = DBConnection.getDBConnection();
                    conn.createStatement().executeUpdate("DELETE FROM CustomerServices WHERE Customer_ID = "+id);
                    conn.createStatement().executeUpdate("DELETE FROM CustomerLocations WHERE Customer_ID = "+id);
                    ResultSet res = DBConnection.getDBConnection().createStatement().executeQuery("SELECT Job_ID FROM Jobs WHERE Customer_ID = "+id);
                    if(res != null && res.next()){
                        Integer jId = res.getInt("Job_ID");
                        conn.createStatement().executeUpdate("DELETE FROM JobServices WHERE Job_ID = "+jId);
                        conn.createStatement().executeUpdate("DELETE FROM Invoices WHERE Job_ID = "+jId);
                    }
                    conn.createStatement().executeUpdate("DELETE FROM Jobs WHERE Customer_ID = "+id);
                    conn.createStatement().executeUpdate("DELETE FROM Customers WHERE Customer_ID = "+id);
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(BillingController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            data.removeAll(objects);
        }
        tableView.getSelectionModel().clearSelection();
       
    }
    public void addNewCustomerClicked(){
        Stage stage = new Stage();
        Parent root=null;
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("EditCustomer.fxml"));     
            root = (Parent)fxmlLoader.load();  
            EditCustomerController controller = fxmlLoader.<EditCustomerController>getController();
                
            JFXDecorator decorator = new JFXDecorator(stage, root);
            decorator.setCustomMaximize(false);
            Scene scene = new Scene(decorator);   
            scene.getStylesheets().add(LawnCareSystem.class.getResource("/resources/css/style.css").toExternalForm());
            stage.setScene(scene);
            stage.setResizable(false);
            controller.setCustomer(null);
           // stage.setMaximized(true);
            stage.setOnHidden(new EventHandler<WindowEvent>() {
                    @Override
                    public void handle(WindowEvent event) {
                        if(controller.infoChangesMade){
                            Alert alert = new Alert(AlertType.CONFIRMATION, "Would you like to save the changes made to this customer?",
                                 ButtonType.YES, 
                                 ButtonType.NO);
                            alert.setTitle("Confirm Changes");
                            alert.setHeaderText(null);
                            Optional<ButtonType> result = alert.showAndWait();
                            if (result.get() == ButtonType.YES) {
                                controller.infoSaveChanges(null);
                                //refreshTable();
                            }
                        }
                        refreshTable();
                    }
                });
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(CustomersController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void customerRowClicked(CustomerRow customerRow){
        Stage stage = new Stage();
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("EditCustomer.fxml"));     
            Parent root = (Parent)fxmlLoader.load();  
            EditCustomerController controller = fxmlLoader.<EditCustomerController>getController();
            controller.setCustomer(customerRow);
            
            JFXDecorator decorator = new JFXDecorator(stage, root);
            decorator.setCustomMaximize(false);
            
            Scene scene = new Scene(decorator);   
            scene.getStylesheets().add(LawnCareSystem.class.getResource("/resources/css/style.css").toExternalForm());
            stage.setScene(scene);
            stage.setResizable(false);
            stage.setOnHidden(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    if(controller.infoChangesMade){
                        Alert alert = new Alert(AlertType.CONFIRMATION, "Would you like to save the changes made to this customer?",
                             ButtonType.YES, 
                             ButtonType.NO);
                        alert.setTitle("Confirm Changes");
                        alert.setHeaderText(null);
                        Optional<ButtonType> result = alert.showAndWait();
                        if (result.get() == ButtonType.YES) {
                            controller.infoSaveChanges(customerRow);
                            //refreshTable();
                        }
                    }
                    refreshTable();
                }
            });
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(CustomersController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    public void emailButtonClicked(){
    
            Stage stage = new Stage();
            Parent root=null;
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("EmailCustomer.fxml"));     
            root = (Parent)fxmlLoader.load();  
            EmailCustomerController controller = fxmlLoader.<EmailCustomerController>getController();
            
            ObservableList l = tableView.getSelectionModel().getSelectedCells();
            if(l.size()>0){
                    int row = ((TablePosition) l.get(0) ).getRow();
                    String emailRec = (String)col_email.getCellData(row);
                for(int i=1; i<l.size(); ++i){
                    row = ((TablePosition) l.get(i) ).getRow();
                    emailRec = emailRec+","+(String)col_email.getCellData(row);
                }
                controller.setToField(emailRec);
            }
            
            JFXDecorator decorator = new JFXDecorator(stage, root);
            decorator.setCustomMaximize(false);
            Scene scene = new Scene(decorator);   
            scene.getStylesheets().add(LawnCareSystem.class.getResource("/resources/css/style.css").toExternalForm());
            stage.setScene(scene);
            stage.setResizable(false);
           // stage.setMaximized(true);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(CustomersController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
