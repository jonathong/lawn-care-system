/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lawncaresystem.gui.Customers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.util.converter.LocalDateStringConverter;
import lawncaresystem.Database.DBConnection;
import lawncaresystem.Util.LogUtil;
import lawncaresystem.gui.ServicesRow;

/**
 * FXML Controller class
 *
 * @author jonathongray
 */


public class ManageCustomerServicesController implements Initializable {

@FXML
JFXButton saveButton;
@FXML
JFXButton scheduleButton;
@FXML
JFXButton removeServButton;
@FXML
JFXTextField priceField;
@FXML
JFXTextField intervalField;
@FXML
TableView servicesTable;
@FXML
DatePicker nextDatePicker;
@FXML
JFXComboBox<Label> servicePicker;

private TableColumn col_serviceId = new TableColumn("ID");
private TableColumn col_name = new TableColumn("Name");
private TableColumn col_price = new TableColumn("Price");
private TableColumn col_delay = new TableColumn("Interval");
private TableColumn col_nextDate = new TableColumn("NextDate");
    

boolean serviceLoaded = false;
boolean changesMade = false;
CustomerRow customer = null;
String address = null;
private final ObservableList<CustomerServiceRow> data = FXCollections.observableArrayList();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO

    }    
    public void setCustomer(CustomerRow c){
        customer = c;
        initializeColumns();
        populateDataset();
        loadServicePicker();
        
        
        removeServButton.setDisable(true); removeServButton.setVisible(false);
    }
    public void setAddress(String a){
        address = a;
    }
    public void loadServiceFields(CustomerServiceRow csr){
        priceField.setText(csr.getPrice());
        intervalField.setText(csr.getInterval());
        nextDatePicker.setValue(csr.getNextDate());
        
        ObservableList<Label> items = servicePicker.getItems();
        Label servicePicked = null;
        for(int i=0; i<items.size(); ++i){
            if(items.get(i).getText().startsWith(csr.getServiceID()+".")){
                servicePicked = items.get(i);
                break;
            }
        }
        if(servicePicked != null)
            servicePicker.getSelectionModel().select(servicePicked);
        
        serviceLoaded = true;
        servicePicker.setDisable(true);
        scheduleButton.setDisable(true); scheduleButton.setVisible(false);
        removeServButton.setDisable(false); removeServButton.setVisible(true);
    }
    public void loadServicePicker(){
        servicePicker.getItems().clear();
            try{
                Connection conn = DBConnection.getDBConnection();
                Statement statement = conn.createStatement();
                ResultSet res = statement.executeQuery("SELECT * FROM Services WHERE Active != 0");
                while(res.next()){
                    String labelStr = res.getString("Service_ID")+". "+res.getString("ServiceType");
                    servicePicker.getItems().add(new Label(labelStr));
                }
                conn.close();
            }catch(SQLException sqle){
                
            }
    }
    public void saveButtonClicked(){
        
        if(changesMade && !servicePicker.getSelectionModel().isEmpty() && !intervalField.getText().equals("")){
            // update CustomerServices
            CustomerServiceRow csr = (CustomerServiceRow)servicesTable.getSelectionModel().getSelectedItem();
            StringBuilder sb = new StringBuilder(100);
            sb.append("UPDATE CustomerServices set");
            if(!priceField.getText().equals(csr.getPrice())){
                // TODO: check to see if price is valid ( a number )
                sb.append(" Price = "+priceField.getText()+",");
            }
            if(!intervalField.getText().equals(csr.getInterval())){
                // TODO: check to see if interval is valid
                sb.append(" Delay = "+intervalField.getText()+",");
            }
            if(!nextDatePicker.getValue().equals(csr.getNextDate())){
                sb.append(" NextDate = '"+nextDatePicker.getValue().toString()+"',");
            }
            sb.deleteCharAt(sb.length()-1);
            
            if(!sb.toString().equals("UPDATE CustomerServices se")){ // check that changes actually occured
            
                sb.append(" where Customer_ID = "+customer.getCustomerID()+" AND Service_ID = "+csr.getServiceID());
                System.out.println(sb.toString());
                Connection conn = DBConnection.getDBConnection();
                Statement statement;
                try {
                    statement = conn.createStatement();
                    statement.executeUpdate(sb.toString());
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ManageCustomerServicesController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            data.clear(); populateDataset();
            
            serviceLoaded = false;
            changesMade = false;
            priceField.setText("");
            intervalField.setText("");
            servicePicker.getSelectionModel().clearSelection(); servicePicker.setValue(new Label(""));
            nextDatePicker.setValue(null);
            servicePicker.setDisable(false);
            scheduleButton.setDisable(false); scheduleButton.setVisible(true);
            removeServButton.setDisable(true); removeServButton.setVisible(false);
        }
    }
    public void scheduleButtonClicked(){
        if(!serviceLoaded && !priceField.getText().equals("") && !intervalField.getText().equals("")){
            if(!servicePicker.getSelectionModel().isEmpty() && !nextDatePicker.getValue().equals(null)){

                StringBuilder sb = new StringBuilder("INSERT INTO CustomerServices(Customer_ID, Address, Service_ID, Price, Delay, NextDate)VALUES(");
                sb.append(customer.getCustomerID()).append(", '").append(address).append("', ");
                String label = servicePicker.getValue().getText();
                sb.append(label.substring(0, label.indexOf("."))).append(", ");
                sb.append(priceField.getText()).append(", ").append(intervalField.getText()).append(", '");
                sb.append(nextDatePicker.getValue().toString()).append("')");
                System.out.println(sb.toString());

                try {
                    Connection conn = DBConnection.getDBConnection();
                    conn.createStatement().execute(sb.toString());
                    conn.close();
                }catch(SQLException sqle){

                }
                data.clear(); populateDataset();
                
                serviceLoaded = false;
                changesMade = false;
                priceField.setText("");
                intervalField.setText("");
                servicePicker.getSelectionModel().clearSelection(); servicePicker.setValue(new Label(""));
                nextDatePicker.setValue(null);
                servicePicker.setDisable(false);
                scheduleButton.setDisable(false); scheduleButton.setVisible(true);
                removeServButton.setDisable(true); removeServButton.setVisible(false);
            }
        }
    }
    public void removeButtonClicked(){
        if(serviceLoaded){
            try {
                CustomerServiceRow csr = (CustomerServiceRow)servicesTable.getSelectionModel().getSelectedItem();
                System.out.println("here");
                
                Connection conn = DBConnection.getDBConnection();
                Statement statement = conn.createStatement();
                statement.executeUpdate("DELETE FROM CustomerServices WHERE Customer_ID = "+customer.getCustomerID()+" AND Service_ID = "+csr.getServiceID());
                conn.close();
                
                data.clear(); populateDataset();
                serviceLoaded = false;
                changesMade = false;
                priceField.setText("");
                intervalField.setText("");
                servicePicker.getSelectionModel().clearSelection(); servicePicker.setValue(new Label(""));
                nextDatePicker.setValue(null);
                servicePicker.setDisable(false);
                scheduleButton.setDisable(false); scheduleButton.setVisible(true);
                removeServButton.setDisable(true); removeServButton.setVisible(false);
            } catch (SQLException ex) {
                Logger.getLogger(ManageCustomerServicesController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    private void populateDataset(){

        String query = "SELECT * FROM CustomerServices WHERE Customer_ID = "+customer.getCustomerID() + " AND Address = '"+address+"'";

        try{
            Connection conn = DBConnection.getDBConnection();
            Statement statement = conn.createStatement();
            ResultSet csRes = statement.executeQuery(query);
            while(csRes.next()){
                String serviceID = csRes.getString("Service_ID");
                ResultSet sRes = conn.createStatement().executeQuery("SELECT * FROM Services WHERE Service_ID = "+serviceID);
                sRes.next();
                data.add(new CustomerServiceRow(serviceID,
                        sRes.getString("ServiceType"),
                        csRes.getString("Price"),
                        csRes.getString("Delay"),
                        LocalDate.parse((csRes.getDate("NextDate").toString()))
                    )
                );
                sRes.close();
            }
            csRes.close();
            conn.close();
        }catch(SQLException sqle){
            LogUtil.getLogger().e("CustomerController#populateTables", "SQL Exception thrown while querying for Customer and Invoice data");
            sqle.printStackTrace();
        }

        servicesTable.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override 
            public void handle(MouseEvent event) {
                if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
                    Node node = ((Node) event.getTarget()).getParent();
                    TableRow row;

                    if (node instanceof TableRow) {
                        row = (TableRow) node;
                    } else {
                        // clicking on text part
                        row = (TableRow) node.getParent();
                    }
                    CustomerServiceRow r = (CustomerServiceRow)row.getItem();
                    loadServiceFields(r);
                    System.out.println(row.getItem());
                }
            }
        });

        servicesTable.setItems(data);
    }
    private void initializeColumns(){
        
        //col_serviceId.setMinWidth(25);
        //col_serviceId.setPrefWidth(40);
        //col_serviceId.setCellValueFactory(new PropertyValueFactory<CustomerServiceRow, String>("serviceID"));
        
        col_name.setMinWidth(50);
        col_name.setPrefWidth(100);
        col_name.setCellValueFactory(new PropertyValueFactory<CustomerServiceRow, String>("name"));
        
        col_price.setMinWidth(30);
        col_price.setPrefWidth(45);
        col_price.setCellValueFactory(new PropertyValueFactory<CustomerServiceRow, String>("price"));
        
        col_delay.setMinWidth(50);
        col_delay.setPrefWidth(75);
        col_delay.setCellValueFactory(new PropertyValueFactory<CustomerServiceRow, Integer>("interval"));
        
        col_nextDate.setMinWidth(50);
        col_nextDate.setPrefWidth(85);
        col_nextDate.setCellValueFactory(new PropertyValueFactory<CustomerServiceRow, LocalDate>("nextDate"));
        
        servicesTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        servicesTable.getColumns().addAll( col_name, col_price, col_delay, col_nextDate);
        //tableView.setTableMenuButtonVisible(true);
        servicesTable.setColumnResizePolicy(TableView.UNCONSTRAINED_RESIZE_POLICY);
    }
    public void changesMade(){
        if(serviceLoaded)
            changesMade = true;
    }
}
