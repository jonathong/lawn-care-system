/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lawncaresystem.gui.Customers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDecorator;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXSnackbar;
import com.jfoenix.controls.JFXSnackbar.SnackbarEvent;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javax.management.Notification;
import lawncaresystem.Database.DBConnection;
import lawncaresystem.LawnCareSystem;
import lawncaresystem.gui.EditServiceController;



/**
 * FXML Controller class
 *
 * @author jonathongray
 * @author Thomas Avery
 */



public class EditCustomerController implements Initializable {

@FXML
JFXTabPane tabPane;
@FXML
Tab locTab;

//Customer Info Tab    
@FXML
JFXTextField cCustomerID, cFName, cLName, cPhoneNumber, cEMail, cStreet, cState, cCity, cZipCode, cNotes;
@FXML
JFXButton cSaveButton;

//Locations Tab
@FXML
JFXListView<Label> locationsList;


@FXML
JFXTextField lAddress;
@FXML
JFXButton lSaveButton;
@FXML
JFXButton lAddLocation;
@FXML
JFXTextField lSquareFt;
@FXML
JFXTextField lGrassType;
@FXML
JFXTextField lNote;
@FXML
JFXButton lServicesButton;
// address -> (grasstype, squareft, note);
HashMap<String, String[]> locationMap = new HashMap<>();

//Services Tab
@FXML
JFXButton sSaveButton;

CustomerRow customerRow = null;
public boolean infoChangesMade = false;
public boolean locChangesMade = false, locLoaded = false;
public boolean servChangesMade = false;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        locationsList.setExpanded(true);
    }
    
    public void setCustomer(CustomerRow customer){

        
        Connection conn = DBConnection.getDBConnection();
        Statement statement;
        try {
            statement = conn.createStatement();
            ResultSet res = statement.executeQuery("SELECT * FROM Services");
            while(res.next()){
                
            }
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(EditCustomerController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        lServicesButton.setDisable(true);
        lServicesButton.setVisible(false);
        
        customerRow = customer;
        if(customerRow!=null){
            // Customer Info
            cCustomerID.setText(customerRow.getCustomerID()+"");
            cEMail.setText(customerRow.getEmail());
            cFName.setText(customerRow.getFirstName());
            cLName.setText(customerRow.getLastName());
            cPhoneNumber.setText(customerRow.getPhoneNumber());
            cState.setText(customerRow.getState());
            cCity.setText(customerRow.getCity());
            cStreet.setText(customerRow.getStreet());
            cZipCode.setText(customerRow.getZipCode());
            if(customerRow.getNote().equals("NULL"))
                cNotes.setText("");
            else
                cNotes.setText(customerRow.getNote());
            
            // Customer Locations
            setUpLocationList();
        }else{
            // new customer.
            // prevent the use of other tabs until a customer is created.
            System.out.println("New Customer");
            locTab.setDisable(true);
        }
    }

    public void setUpLocationList(){
        Connection conn = DBConnection.getDBConnection();
        try {
            ResultSet res = conn.createStatement().executeQuery("SELECT * FROM CustomerLocations WHERE Customer_ID = "+customerRow.getCustomerID());
            while(res.next()){
                String address = res.getString("Address");
                String[] locInfo = new String[3];
                locInfo[0] = res.getString("GrassType");
                locInfo[1] = res.getString("SquareFt")+"";
                locInfo[2] = res.getString("Note");
                locationMap.put(address, locInfo);
                locationsList.getItems().add(new Label(address));
            }
        } catch (SQLException ex) {
            Logger.getLogger(EditCustomerController.class.getName()).log(Level.SEVERE, null, ex);
        }
        locationsList.setOnMouseClicked(new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent event){
                if( event.getClickCount()==2 ){
                    if(locChangesMade && locLoaded){ // prompt user to save changes first.
                        // prompt user to save changes
                        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Would you like to save the changes made to "
                                +((Label)locationsList.getSelectionModel().getSelectedItems().get(0)).getText()
                                +" ?",
                            ButtonType.YES, 
                            ButtonType.NO);
                        alert.setTitle("Confirm Changes");
                        alert.setHeaderText(null);
                        Optional<ButtonType> result = alert.showAndWait();
                        if (result.get() == ButtonType.YES) {
                            locSaveChanges();
                        }
                        locChangesMade = false;
                    }else{ //changes were not made, so it is okay to load another location into fields.
                        String text = ((Label)locationsList.getSelectionModel().getSelectedItems().get(0)).getText();
                        if(text != null){
                            loadLocationFields(text);
                        }
                    }
                }else if( event.getClickCount()==1){
                    if(locChangesMade && locLoaded){
                        // prompt user to save changes
                        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Would you like to save the changes made to "
                                +((Label)locationsList.getSelectionModel().getSelectedItems().get(0)).getText()
                                +" ?",
                            ButtonType.YES, 
                            ButtonType.NO);
                        alert.setTitle("Confirm Changes");
                        alert.setHeaderText(null);
                        Optional<ButtonType> result = alert.showAndWait();
                        if (result.get() == ButtonType.YES) {
                            locSaveChanges();
                        }
                        locChangesMade = false;
                    }
                }
            }
        });
    }
    public void loadLocationFields(String text){
        String[] lInfo = locationMap.get(text);
        lAddress.setText(text);
        lGrassType.setText(lInfo[0]);
        lSquareFt.setText(lInfo[1]);
        lNote.setText(lInfo[2]);
        locLoaded = true;
        lAddLocation.setDisable(true);
        lAddLocation.setVisible(false);
        lServicesButton.setDisable(false);
        lServicesButton.setVisible(true);
    }
    public void infoSaveChanges(CustomerRow cr){
        
        if(!cCustomerID.getText().equals("NEW") && cr!=null){
            StringBuilder sb = new StringBuilder(100);
            sb.append("update Customers");
            sb.append(" set");
            if(!cFName.getText().equals(cr.getFirstName())){
                sb.append(" FirstName = '"+cFName.getText()+"',");
            }
            if(!cLName.getText().equals(cr.getLastName())){
                sb.append(" LastName = '"+cLName.getText()+"',");
            }
            if(!cEMail.getText().equals(cr.getEmail())){
                sb.append(" Email = '"+cEMail.getText()+"',");
            }
            if(!cPhoneNumber.getText().equals(cr.getPhoneNumber())){
                sb.append(" PhoneNumber = '"+cPhoneNumber.getText()+"',");
            }
            if(!cCity.getText().equals(cr.getCity())){
                sb.append(" City = '"+cCity.getText()+"',");
            }
            if(!cStreet.getText().equals(cr.getStreet())){
                sb.append(" Street = '"+cStreet.getText()+"',");
            }
            if(!cState.getText().equals(cr.getState())){
                sb.append(" State = '"+cState.getText()+"',");
            }
            if(!cZipCode.getText().equals(cr.getZipCode())){
                sb.append(" ZipCode = "+cZipCode.getText()+",");
            }
            if(!cNotes.getText().equals(cr.getNote())){
                sb.append(" Note = '"+cNotes.getText().replace("'", "")+"',");
            }
            sb.deleteCharAt(sb.length()-1);
            
            if(!sb.toString().equals("update Customers se")){ // check that changes actually occured
            
                sb.append(" where Customer_ID = "+cCustomerID.getText());
                System.out.println(sb.toString());
                Connection conn = DBConnection.getDBConnection();
                Statement statement;
                try {
                    statement = conn.createStatement();
                    statement.executeUpdate(sb.toString());
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(EditCustomerController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }else{
            // must be a new entry
            DBConnection.insertIntoTable(cLName.getText(),
                    cFName.getText(),
                    cPhoneNumber.getText(),
                    cEMail.getText(),
                    cStreet.getText(),
                    cCity.getText(),
                    cState.getText(),
                    Integer.parseInt(cZipCode.getText()),
                    cNotes.getText().replace("'", ""));
        }
        
        infoChangesMade = false;
    }
    public void locSaveChanges(){
        StringBuilder sb = new StringBuilder("UPDATE CustomerLocations set");
        if(!locationsList.getSelectionModel().getSelectedIndices().isEmpty()){
            String address = ((Label)locationsList.getSelectionModel().getSelectedItems().get(0)).getText();
            String[] locInfo = locationMap.get(address);

            if(!address.equals(lAddress.getText())){
                sb.append(" Address = '").append(lAddress.getText()).append("',");
            }
            if(!locInfo[0].equals(lGrassType.getText())){
                sb.append(" GrassType = '").append(lGrassType.getText()).append("',");
            }
            if(!locInfo[1].equals(lSquareFt.getText())){
                if(lSquareFt.getText().equals("")){
                    sb.append(" SquareFt = ").append("NULL").append(",");
                }else if(lSquareFt.getText().toLowerCase().equals("null")){
                    sb.append(" SquareFt = ").append("NULL").append(",");                    
                }else{
                    sb.append(" SquareFt = ").append(lSquareFt.getText()).append(",");
                }
            }
            if(!locInfo[2].equals(lNote.getText())){
                sb.append(" Note = '").append(lNote.getText()).append("',");
            }
            sb.deleteCharAt(sb.length()-1);
            if(!sb.toString().equals("UPDATE CustomerLocations se")){ // check that changes actually occured

                sb.append(" where Customer_ID = "+cCustomerID.getText()+" AND Address = '"+address+"'");
                System.out.println(sb.toString());
                Connection conn = DBConnection.getDBConnection();
                Statement statement;
                try {
                    statement = conn.createStatement();
                    statement.executeUpdate(sb.toString());
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(EditCustomerController.class.getName()).log(Level.SEVERE, null, ex);
                }
                locationsList.getItems().removeAll(locationsList.getItems());
                setUpLocationList();
                locChangesMade = false;
                lAddress.setText(""); lGrassType.setText(""); lSquareFt.setText(""); lNote.setText("");
                locLoaded = false;
                lAddLocation.setDisable(false);
                lAddLocation.setVisible(true);
                lServicesButton.setDisable(true);
                lServicesButton.setVisible(false);
            }
        }
    }
    public void addNewLocationButtonClicked(){
        if(!locLoaded && !lAddress.equals("")){
            StringBuilder sb = new StringBuilder("INSERT INTO CustomerLocations(Customer_ID, Address, SquareFt, GrassType, Note)VALUES(");
            sb.append(customerRow.getCustomerID()).append(", '").append(lAddress.getText()).append("',");
            if(!lSquareFt.getText().equals("")){
                sb.append(lSquareFt.getText()).append(",'");
            }else if(lSquareFt.getText().toLowerCase().equals("null")){
                sb.append("NULL,'");
            }else{
                sb.append("NULL,'");
            }
            sb.append(lGrassType.getText()).append("','").append(lNote.getText()).append("')");
            System.out.println(sb.toString());
            
            try {
                Connection conn = DBConnection.getDBConnection();
                conn.createStatement().execute(sb.toString());
                conn.close();
                
                locationsList.getItems().removeAll(locationsList.getItems());
                setUpLocationList();
                locChangesMade = false;
                lAddress.setText(""); lGrassType.setText(""); lSquareFt.setText(""); lNote.setText("");
                locLoaded = false;
                lAddLocation.setDisable(false);
                lAddLocation.setVisible(true);
            } catch (SQLException ex) {
                Logger.getLogger(EditServiceController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
    }
    public void servicesButtonClicked(){
        Stage stage = new Stage();
        Parent root=null;
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("ManageCustomerServices.fxml"));     
            root = (Parent)fxmlLoader.load();  
            ManageCustomerServicesController controller = fxmlLoader.<ManageCustomerServicesController>getController();
            controller.setAddress(((Label)locationsList.getSelectionModel().getSelectedItems().get(0)).getText());
            controller.setCustomer(customerRow);
            JFXDecorator decorator = new JFXDecorator(stage, root);
            decorator.setCustomMaximize(false);
            Scene scene = new Scene(decorator);   
            scene.getStylesheets().add(LawnCareSystem.class.getResource("/resources/css/style.css").toExternalForm());
            stage.setScene(scene);
            stage.setResizable(false);
           // stage.setMaximized(true);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(CustomersController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void servSaveChanges(){
        
        
    }
    public void customerInfoChanged(){
        infoChangesMade = true;
    }
    public void customerLocChanged(){
        locChangesMade = true;
    }
    public void customerServChanged(){
        servChangesMade = true;
    }
    public void infoSaveButtonClicked(){
        infoSaveChanges(customerRow);
    }
    public void locSaveButtonClicked(){
        locSaveChanges();
    }
    public void servSaveButtonClicked(){
        servSaveChanges();
    }
}
