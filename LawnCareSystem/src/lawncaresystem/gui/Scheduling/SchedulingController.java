/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lawncaresystem.gui.Scheduling;

import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;



        

/**
 * FXML Controller class
 *
 * @author Jonathon
 */
public class SchedulingController implements Initializable {

    
    @FXML
    JFXButton todayButton = new JFXButton();
    @FXML
    JFXButton completeJobsButton = new JFXButton();
    @FXML
    JFXButton finishDayButton = new JFXButton();
    @FXML
    JFXButton createDailyRouteButton = new JFXButton();
    @FXML
    DatePicker date = new DatePicker();
    
    /**
     * Initializes the controller class.
     */
    @Override
   

    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
   
}
