/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lawncaresystem.gui;

import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import lawncaresystem.Database.DBConnection;

/**
 * FXML Controller class
 *
 * @author jonathongray
 */
public class EditServiceController implements Initializable {

    boolean changesMade = false;
    @FXML
    JFXTextField serviceName;
    @FXML
    JFXTextField servicePrice;
    
    String prevName = null;
    String prevPrice = null;
    String id = null;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        if(prevName != null)
            serviceName.setText(prevName);
        if(prevPrice != null)
            servicePrice.setText(prevPrice);
        // TODO
    }
    public void saveChanges(){
        System.out.println("prevName = "+prevName+" prevPrice = "+prevPrice);
        System.out.println("name = "+serviceName.getText()+" price = "+servicePrice.getText());
        if(prevName!=null){ // editing a service
            System.out.println("updating");
            String update = "update Services set";
            if(!prevName.equals(serviceName.getText())){
                update = update + " ServiceType = '"+serviceName.getText()+"',";
            }if(!prevPrice.equals(servicePrice.getText())){
                update = update + " Price = "+servicePrice.getText()+",";
            }
            update = update.substring(0, update.length()-1);
            update = update + " where Service_ID = "+id;
            System.out.println(update);
            try {
                Connection conn = DBConnection.getDBConnection();
                conn.createStatement().execute(update);
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(EditServiceController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }else{ // new service
            System.out.println("New Service");
            if(!serviceName.getText().equals("")){
                String insert = "INSERT INTO Services(ServiceType, Price)VALUES('"+serviceName.getText()+"',";
                if(!servicePrice.getText().equals(""))
                    insert = insert+" "+servicePrice.getText()+")";
                else
                    insert = insert+" 0.0"+")";
                
                try {
                    Connection conn = DBConnection.getDBConnection();
                    conn.createStatement().execute(insert);
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(EditServiceController.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }
        changesMade = false;
    }
    public void saveButtonClicked(){
        saveChanges();
    }
    public void onChangeMade(){
        changesMade = true;
    }
    public void setName(String name){
        prevName = name;
        serviceName.setText(prevName);
    }
    public void setPrice(String price){
        prevPrice = price;
        servicePrice.setText(prevPrice);
    }
    public void setID(String id){
        this.id = id;
    }
    
}
