/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lawncaresystem.gui.Billing;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Jonathon
 */
public class BillingRow {
    private final SimpleIntegerProperty customerID;
    private final SimpleStringProperty lastName;
    private final SimpleStringProperty firstName;
    private final SimpleStringProperty jobID;
    private final SimpleStringProperty subtotal;
    private final SimpleStringProperty taxPercent;
    private final SimpleStringProperty total;
    private final SimpleStringProperty dateSent;
    private final SimpleStringProperty dateDue;
    private final SimpleStringProperty datePaid;
    
    
    public BillingRow(String cID, String lName, String fName, String jID, String sub,
            String tax, String tot, String dSent, String dDue, String dPaid){
        
        this.customerID = new SimpleIntegerProperty(Integer.parseInt(cID));
        this.lastName = new SimpleStringProperty(lName);
        this.firstName = new SimpleStringProperty(fName);
        this.jobID = new SimpleStringProperty(jID);
        this.subtotal = new SimpleStringProperty(sub);
        this.taxPercent = new SimpleStringProperty(tax);
        this.total = new SimpleStringProperty(tot);
        this.dateSent = new SimpleStringProperty(dSent);
        this.dateDue = new SimpleStringProperty(dDue);
        this.datePaid = new SimpleStringProperty(dPaid);
    }

    public Integer getCustomerID() {
        return customerID.get();
    }

    public String getLastName() {
        return lastName.get();
    }

    public String getFirstName() {
        return firstName.get();
    }

    public String getJobID() {
        return jobID.get();
    }
    
    public String getSubtotal(){
        return subtotal.get();
    }
    
    public String getTaxPercent(){
        return taxPercent.get();
    }
    
    public String getTotal() {
        return total.get();
    }

    public String getDateSent() {
        return dateSent.get();
    }

    public String getDateDue() {
        return dateDue.get();
    }

    public String getDatePaid() {
        return datePaid.get();
    }
    
    public void setCustomerID(String id){
        customerID.set(Integer.parseInt(id));
    }
    
    public void setLastName(String n){
        lastName.set(n);
    }
    
    public void setFirstName(String n){
        firstName.set(n);
    }
    
    public void setJobID(String id){
        jobID.set(id);
    }
    
    public void setSubtotal(String sub){
        subtotal.set(sub);
    }
    
    public void setTaxPercent(String tax){
        taxPercent.set(tax);
    }
    
    public void setTotal(String t){
        total.set(t);
    }
    
    public void setDateSent(String date){
        dateSent.set(date);
    }
    
    public void setDateDue(String date){
        dateDue.set(date);
    }
    
    public void setDatePaid(String date){
        datePaid.set(date);
    }

    
}
