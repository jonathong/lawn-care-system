/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lawncaresystem.gui.Billing;

import com.jfoenix.controls.JFXDecorator;
import com.jfoenix.controls.JFXTextField;
import com.sun.jnlp.ApiDialog;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import lawncaresystem.Database.DBConnection;
import lawncaresystem.Database.DBHelper;
import lawncaresystem.LawnCareSystem;
import lawncaresystem.Util.LogUtil;
import lawncaresystem.gui.Customers.EmailCustomerController;


/**
 * FXML Controller class
 *
 * @author Jonathon
 * @author Thomas
 */
public class BillingController implements Initializable {

    @FXML
    Text outstandingText;
    
    @FXML
    JFXTextField filterField;
    
    @FXML
    public TableView tableView;
    
    private TableColumn col_customerID = new TableColumn("Customer_ID");
    private TableColumn col_lastName = new TableColumn("LastName");
    private TableColumn col_firstName = new TableColumn("FirstName");
    private TableColumn col_jobID = new TableColumn("Job_ID");
    private TableColumn col_subtotal = new TableColumn("Subtotal");
    private TableColumn col_taxPercent = new TableColumn("Tax");
    private TableColumn col_total = new TableColumn("Total");
    private TableColumn col_dateSent = new TableColumn("DateSent");
    private TableColumn col_dateDue = new TableColumn("DateDue");
    private TableColumn col_datePaid = new TableColumn("DatePaid");
    
    private final ObservableList<BillingRow> data = FXCollections.observableArrayList();
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        // First, initialize the table and columns
        tableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        initializeColumns();
        populateTable();
        totalOutStanding();
        
        FilteredList<BillingRow> filteredData = new FilteredList<>(data, e -> true);
        filterField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(bill -> {
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                String lowerCaseFilter = newValue.toLowerCase();

                if (bill.getFirstName().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches first name.
                } else if (bill.getLastName().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                } else if ((bill.getCustomerID()+"").contains(lowerCaseFilter) ){
                    return true;
                } else if(bill.getDateDue().toLowerCase().contains(lowerCaseFilter)){
                    return true;
                } else if(bill.getDatePaid()!=null && bill.getDatePaid().toLowerCase().contains(lowerCaseFilter)){
                    return true;
                } else if( (bill.getJobID()+"").contains(lowerCaseFilter) ){
                    return true;
                }
                return false; // Does not match.
            });
        });

        SortedList<BillingRow> sortedData = new SortedList<>(filteredData);
        sortedData.comparatorProperty().bind(tableView.comparatorProperty());
        tableView.setItems(sortedData);
       
    }
    private void populateTable(){
        
        String query = "SELECT j.Customer_ID,j.Job_ID,LastName,FirstName, Subtotal, TaxPercent, Total, DateSent, DateDue,DatePaid FROM Jobs as j inner join Customers as c on c.Customer_ID = j.Customer_ID inner join Invoices as i on j.Job_ID = i.Job_ID";
      //String query2 = "SELECT Customers.Customer_ID, Jobs.Job_ID, Customers.LastName, Customers.FirstName, Invoices.Total, Invoices.DateSent, Invoices.DateDue, Invoices.DatePaid FROM Invoices, Customers, Jobs WHERE Jobs.Job_ID=Invoices.Job_ID AND Jobs.Customer_ID = Customers.Customer_ID";

        try{
            Connection conn = DBConnection.getDBConnection();
            Statement statement = conn.createStatement();
            ResultSet res = statement.executeQuery(query);
            while(res.next()){
                System.out.println("\t"+res.getString("Customer_ID")+" - "+res.getString("LastName")+" - "+res.getString("FirstName")+" - "+res.getString("Job_ID")+" - "+res.getString("Total"));
                data.add(new BillingRow(
                        res.getString("Customer_ID"),
                        res.getString("LastName"),
                        res.getString("FirstName"),
                        res.getString("Job_ID"),
                        res.getString("Subtotal"),
                        res.getString("TaxPercent"),
                        res.getString("Total"),
                        res.getString("DateSent"),
                        res.getString("DateDue"),
                        res.getString("DatePaid")
                ));
                //tableView.setItems(data);
            }
            res.close();
            conn.close();
        }catch(SQLException sqle){
            LogUtil.getLogger().e("BillingController#populateTables", "SQL Exception thrown while querying for Customer and Invoice data");
            sqle.printStackTrace();
        }
    }
    private void initializeColumns(){
        col_customerID.setMinWidth(80);
        col_customerID.setPrefWidth(80);
        col_customerID.setCellValueFactory(new PropertyValueFactory<BillingRow, String>("customerID"));
        
        col_lastName.setMinWidth(100);
        col_lastName.setPrefWidth(100);
        col_lastName.setCellValueFactory(new PropertyValueFactory<BillingRow, String>("lastName"));
        
        col_firstName.setMinWidth(100);
        col_firstName.setPrefWidth(100);
        col_firstName.setCellValueFactory(new PropertyValueFactory<BillingRow, String>("firstName"));
        
        col_jobID.setMinWidth(80);
        col_jobID.setPrefWidth(80);
        col_jobID.setCellValueFactory(new PropertyValueFactory<BillingRow, String>("jobID"));
        
        col_subtotal.setMinWidth(40);
        col_subtotal.setPrefWidth(60);
        col_subtotal.setCellValueFactory(new PropertyValueFactory<BillingRow, String>("subtotal"));
        
        col_taxPercent.setMinWidth(30);
        col_taxPercent.setPrefWidth(40);
        col_taxPercent.setCellValueFactory(new PropertyValueFactory<BillingRow, String>("taxPercent"));        
        
        col_total.setMinWidth(40);
        col_total.setPrefWidth(60);
        col_total.setCellValueFactory(new PropertyValueFactory<BillingRow, String>("total"));
        
        col_dateSent.setMinWidth(80);
        col_dateSent.setPrefWidth(80);
        col_dateSent.setCellValueFactory(new PropertyValueFactory<BillingRow, String>("dateSent"));
        
        col_dateDue.setMinWidth(80);
        col_dateDue.setPrefWidth(80);
        col_dateDue.setCellValueFactory(new PropertyValueFactory<BillingRow, String>("dateDue"));
        
        col_datePaid.setMinWidth(80);
        col_datePaid.setPrefWidth(80);
        col_datePaid.setCellValueFactory(new PropertyValueFactory<BillingRow, String>("datePaid"));
        
        tableView.getColumns().addAll(col_jobID, col_customerID, col_lastName, col_firstName
                , col_subtotal, col_taxPercent, col_total, col_dateSent, col_dateDue, col_datePaid);
        tableView.setTableMenuButtonVisible(true);
        
    }
    public void eMailButtonClicked(){
          
    }
     public void alterPriceButtonClicked(){
        System.out.println("Price is now being altered!");
    }
      public void billNowButtonClicked(){
        System.out.println("Customer is being billed!");
    }
    public void deleteBillButtonClicked(){
        ObservableList l = tableView.getSelectionModel().getSelectedCells();
        ObservableList<BillingRow> objects = tableView.getSelectionModel().getSelectedItems();
        
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirm Deletion");
        alert.setHeaderText(null);
        alert.setContentText("Are you sure you want to delete "+l.size()+" invoices from your database? This action cannot be undone.");
        Optional <ButtonType> action = alert.showAndWait();
        
        if(action.get()==ButtonType.OK){
            for(int i=0; i<l.size();++i){
                int row = ((TablePosition) l.get(i) ).getRow();
                String id = (String)col_jobID.getCellData(row);
                try {
                    DBConnection.getDBConnection().createStatement().executeUpdate("DELETE FROM INVOICES WHERE Job_ID = "+id);
                } catch (SQLException ex) {
                    Logger.getLogger(BillingController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            data.removeAll(objects);
        }
        tableView.getSelectionModel().clearSelection();
        
    }
    public void totalOutStanding(){
      /* 
          This is where the SQL is for updating the total outstanding txt field
        SELECT SUM(Total)
        FROM Invoices
        WHEN DatePaid IS NULL; 
      */
      //once computing the the value for the some store in int valueTotal
      
        String firstQuery = "SELECT SUM(Total) as sum FROM Invoices WHERE DatePaid IS NULL";
        try {
            Connection conn = DBConnection.getDBConnection();
            ResultSet res = conn.createStatement().executeQuery(firstQuery);
                if(res.next()){
                    Double value = res.getDouble("sum");
                    outstandingText.setText("$ "+String.valueOf(value));
                }
            conn.close();
        }catch (SQLException ex) {
                Logger.getLogger(BillingController.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }
    
}