/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lawncaresystem.gui.Home;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;

/**
 * FXML Controller class
 *
 * @author Jonathon
 */
public class HomeController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
     public void notifyButton(){
    Notifications notificationBuilder = Notifications.create()
        .title("Notify Title")
        .text("This can be text describing in detail the notification")
        .graphic(null)
        .hideAfter(Duration.INDEFINITE)
        .position(Pos.CENTER_LEFT)
        .onAction(new EventHandler<ActionEvent>(){
           @Override
           public void handle(ActionEvent event){
               System.out.println("User clicked on the Notification!"); 
           }
        });    
     //notificationBuilder.darkStyle();
      notificationBuilder.showConfirm();      
 }
}
