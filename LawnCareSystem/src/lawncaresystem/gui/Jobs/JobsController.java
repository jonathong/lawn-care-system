/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lawncaresystem.gui.Jobs;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDecorator;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import lawncaresystem.Database.DBConnection;
import lawncaresystem.LawnCareSystem;
import lawncaresystem.Util.LogUtil;
import lawncaresystem.gui.Billing.BillingController;
import lawncaresystem.gui.Billing.BillingRow;
import lawncaresystem.gui.Customers.CustomerRow;
import lawncaresystem.gui.Customers.CustomersController;
import lawncaresystem.gui.Customers.EditCustomerController;
import sun.awt.image.ByteBandedRaster;

/**
 * FXML Controller class
 *
 * @author Jonathon
 */
public class JobsController implements Initializable {

    
    private TableColumn col_customerID = new TableColumn("Customer_ID");
    private TableColumn col_lastName = new TableColumn("LastName");
    private TableColumn col_firstName = new TableColumn("FirstName");
    private TableColumn col_jobID = new TableColumn("Job_ID");
    private TableColumn col_subtotal = new TableColumn("Subtotal");
    private TableColumn col_date = new TableColumn("Date");
    private TableColumn col_servicesCount = new TableColumn("Services");
    private TableColumn col_address = new TableColumn("Address");
    
    @FXML
    TableView jobsTable;
    @FXML
    JFXButton addJobButton;
    @FXML
    JFXButton deleteJobButton;
    @FXML
    JFXTextField filterField;
    
    ObservableList<JobRow> data = FXCollections.observableArrayList();
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        initializeColumns();
        populateTable();
        setupSearch();
    }    
    private void setupSearch(){
        FilteredList<JobRow> filteredData = new FilteredList<>(data, e -> true);
        filterField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(job -> {
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                String lowerCaseFilter = newValue.toLowerCase();

                if (job.getFirstName().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches first name.
                } else if (job.getLastName().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                } else if ((job.getCustomerID()+"").contains(lowerCaseFilter) ){
                    return true;
                } else if(job.getDate().toLowerCase().contains(lowerCaseFilter)){
                    return true;
                } else if(job.getAddress().toLowerCase().contains(lowerCaseFilter)){
                    return true;
                } else if( (job.getJobID()+"").contains(lowerCaseFilter) ){
                    return true;
                }else if(job.getServiceCount().contains(lowerCaseFilter)){
                    return true;
                }else if(job.getSubtotal().contains(lowerCaseFilter)){
                    return true;
                }
                return false; // Does not match.
            });
        });

        SortedList<JobRow> sortedData = new SortedList<>(filteredData);
        sortedData.comparatorProperty().bind(jobsTable.comparatorProperty());
        jobsTable.setItems(sortedData);
    }
    private void populateTable(){
        
      //String query2 = "SELECT Customers.Customer_ID, Jobs.Job_ID, Customers.LastName, Customers.FirstName, Invoices.Total, Invoices.DateSent, Invoices.DateDue, Invoices.DatePaid FROM Invoices, Customers, Jobs WHERE Jobs.Job_ID=Invoices.Job_ID AND Jobs.Customer_ID = Customers.Customer_ID";
        String query = "SELECT j.Customer_ID,j.Job_ID, j.Date, j.Address, LastName, FirstName FROM Jobs as j inner join Customers as c on c.Customer_ID = j.Customer_ID";
        String query2 = "SELECT SUM(Price) as sPrice, COUNT(Service_ID) as sCount FROM JobServices WHERE Job_ID = ";
        try{
            Connection conn = DBConnection.getDBConnection();
            Statement statement = conn.createStatement();
            ResultSet res = statement.executeQuery(query);
            while(res.next()){
                String jId = res.getString("Job_ID");
                ResultSet  jsRes = conn.createStatement().executeQuery(query2+jId);
                jsRes.next();
                data.add(new JobRow(
                        res.getString("Customer_ID"),
                        res.getString("LastName"),
                        res.getString("FirstName"),
                        jId,
                        jsRes.getString("sPrice"),
                        res.getString("Date"),
                        jsRes.getString("sCount"),
                        res.getString("Address")
                ));
                jsRes.close();
            }
            jobsTable.setItems(data);
            res.close();
            conn.close();
        }catch(SQLException sqle){
            LogUtil.getLogger().e("JobsController#populateTables", "SQL Exception thrown while querying for Customer and Invoice data");
            sqle.printStackTrace();
        }
        
        jobsTable.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override 
            public void handle(MouseEvent event) {
                if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
                    Node node = ((Node) event.getTarget()).getParent();
                    TableRow row;
                    
                    if (node instanceof TableRow) {
                        row = (TableRow) node;
                    } else {
                        // clicking on text part
                        row = (TableRow) node.getParent();
                    }
                    JobRow j = (JobRow)row.getItem();
                    jobRowClicked(j);
                    System.out.println(row.getItem());
                }
            }
        });
        
    }
    
    private void jobRowClicked(JobRow j){
        Stage stage = new Stage();
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("EditJob.fxml"));     
            Parent root = (Parent)fxmlLoader.load();  
            EditJobController controller = fxmlLoader.<EditJobController>getController();
            controller.setJob(j);
            
            JFXDecorator decorator = new JFXDecorator(stage, root);
            decorator.setCustomMaximize(false);
            
            Scene scene = new Scene(decorator);   
            scene.getStylesheets().add(LawnCareSystem.class.getResource("/resources/css/style.css").toExternalForm());
            stage.setScene(scene);
            stage.setResizable(false);
            stage.setOnHidden(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    if(controller.changesMade){
                        Alert alert = new Alert(AlertType.CONFIRMATION, "Would you like to save the changes made to this customer?",
                             ButtonType.YES, 
                             ButtonType.NO);
                        alert.setTitle("Confirm Changes");
                        alert.setHeaderText(null);
                        Optional<ButtonType> result = alert.showAndWait();
                        if (result.get() == ButtonType.YES) {
                            controller.saveChanges();
                            //refreshTable();
                        }
                    }
                    //refreshTable();
                }
            });
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(CustomersController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void initializeColumns(){
        col_customerID.setMinWidth(80);
        col_customerID.setPrefWidth(80);
        col_customerID.setCellValueFactory(new PropertyValueFactory<BillingRow, String>("customerID"));
        
        col_lastName.setMinWidth(100);
        col_lastName.setPrefWidth(100);
        col_lastName.setCellValueFactory(new PropertyValueFactory<BillingRow, String>("lastName"));
        
        col_firstName.setMinWidth(100);
        col_firstName.setPrefWidth(100);
        col_firstName.setCellValueFactory(new PropertyValueFactory<BillingRow, String>("firstName"));
        
        col_jobID.setMinWidth(80);
        col_jobID.setPrefWidth(80);
        col_jobID.setCellValueFactory(new PropertyValueFactory<BillingRow, String>("jobID"));

        col_subtotal.setMinWidth(40);
        col_subtotal.setPrefWidth(60);
        col_subtotal.setCellValueFactory(new PropertyValueFactory<BillingRow, String>("subtotal"));
        
        col_date.setMinWidth(80);
        col_date.setPrefWidth(80);
        col_date.setCellValueFactory(new PropertyValueFactory<BillingRow, String>("date"));
        
        col_servicesCount.setMinWidth(80);
        col_servicesCount.setPrefWidth(80);
        col_servicesCount.setCellValueFactory(new PropertyValueFactory<BillingRow, String>("serviceCount"));
        
        col_address.setMinWidth(80);
        col_address.setPrefWidth(80);
        col_address.setCellValueFactory(new PropertyValueFactory<BillingRow, String>("address"));
        
        jobsTable.getColumns().addAll(col_jobID, col_customerID, col_lastName, col_firstName
                ,col_address, col_servicesCount, col_subtotal, col_date);
        jobsTable.setTableMenuButtonVisible(true);
        
    }
    
}
