/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lawncaresystem.gui.Jobs;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author jonathongray
 */
public class JobRow {
    private final SimpleIntegerProperty customerID;
    private final SimpleStringProperty lastName;
    private final SimpleStringProperty firstName;
    private final SimpleStringProperty jobID;
    private final SimpleStringProperty subtotal;
    private final SimpleStringProperty date;
    private final SimpleStringProperty serviceCount;
    private final SimpleStringProperty address;
    
    
    public JobRow(String cID, String lName, String fName, String jID,
            String stot, String d, String s, String ad){
        
        this.customerID = new SimpleIntegerProperty(Integer.parseInt(cID));
        this.lastName = new SimpleStringProperty(lName);
        this.firstName = new SimpleStringProperty(fName);
        this.jobID = new SimpleStringProperty(jID);
        this.subtotal = new SimpleStringProperty(stot);
        this.date = new SimpleStringProperty(d);
        this.serviceCount = new SimpleStringProperty(s);
        this.address = new SimpleStringProperty(ad);
    }

    public Integer getCustomerID() {
        return customerID.get();
    }

    public String getLastName() {
        return lastName.get();
    }

    public String getFirstName() {
        return firstName.get();
    }

    public String getJobID() {
        return jobID.get();
    }

    public String getSubtotal() {
        return subtotal.get();
    }

    public String getDate() {
        return date.get();
    }

    public String getServiceCount() {
        return serviceCount.get();
    }

    public String getAddress() {
        return address.get();
    }
    
    public void setCustomerID(String id){
        customerID.set(Integer.parseInt(id));
    }
    
    public void setLastName(String n){
        lastName.set(n);
    }
    
    public void setFirstName(String n){
        firstName.set(n);
    }
    
    public void setJobID(String id){
        jobID.set(id);
    }
    
    public void setSubtotal(String t){
        subtotal.set(t);
    }
    
    public void setDate(String d){
        date.set(d);
    }
    
    public void setServiceCount(String s){
        serviceCount.set(s);
    }
    
    public void setAddress(String a){
        address.set(a);
    }
}
