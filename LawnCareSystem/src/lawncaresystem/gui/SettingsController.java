/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lawncaresystem.gui;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDecorator;
import com.jfoenix.controls.JFXListCell;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXRippler;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import lawncaresystem.Database.DBConnection;
import lawncaresystem.LawnCareSystem;
import lawncaresystem.Util.LogUtil;
import lawncaresystem.Util.MasterControl;
import lawncaresystem.gui.Billing.BillingController;
import lawncaresystem.gui.Customers.CustomerRow;
import lawncaresystem.gui.Customers.CustomersController;
import lawncaresystem.gui.Customers.EditCustomerController;

/**
 * FXML Controller class
 *
 * @author Jonathon
 */
public class SettingsController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    BorderPane borderPane;
    @FXML
    JFXTextField taxPercent;
    @FXML
    TableView tableView;
    
    private TableColumn col_serviceId = new TableColumn("ID");
    private TableColumn col_name = new TableColumn("Name");
    private TableColumn col_price = new TableColumn("Price");
    
    private final ObservableList<ServicesRow> data = FXCollections.observableArrayList();

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        borderPane.getStylesheets().add(LawnCareSystem.class.getResource("/resources/css/Settings.css").toExternalForm());
        tableView.setItems(data);
        initializeColumns();
        populateDataset();
        taxPercent.setText(MasterControl.access().getTaxPercent()+"");
        
    }
    
    private void populateDataset(){
        
        String query = "SELECT * FROM Services WHERE Active != 0";

        try{
            Connection conn = DBConnection.getDBConnection();
            Statement statement = conn.createStatement();
            ResultSet res = statement.executeQuery(query);
            while(res.next()){
                data.add(new ServicesRow(res.getString("Service_ID"), res.getString("ServiceType"), res.getString("Price")));
            }
            res.close();
            conn.close();
        }catch(SQLException sqle){
            LogUtil.getLogger().e("CustomerController#populateTables", "SQL Exception thrown while querying for Customer and Invoice data");
            sqle.printStackTrace();
        }
        
        tableView.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override 
            public void handle(MouseEvent event) {
                if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
                    Node node = ((Node) event.getTarget()).getParent();
                    TableRow row;
                    
                    if (node instanceof TableRow) {
                        row = (TableRow) node;
                    } else {
                        // clicking on text part
                        row = (TableRow) node.getParent();
                    }
                    ServicesRow r = (ServicesRow)row.getItem();
                    editServices(r);
                    System.out.println(row.getItem());
                }
            }
        });
        
    }
    private void initializeColumns(){
        
        col_serviceId.setMinWidth(25);
        col_serviceId.setPrefWidth(40);
        col_serviceId.setCellValueFactory(new PropertyValueFactory<ServicesRow, String>("serviceID"));
        
        col_name.setMinWidth(50);
        col_name.setPrefWidth(100);
        col_name.setCellValueFactory(new PropertyValueFactory<ServicesRow, String>("name"));
        
        col_price.setMinWidth(50);
        col_price.setPrefWidth(100);
        col_price.setCellValueFactory(new PropertyValueFactory<ServicesRow, String>("price"));
        
        tableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        tableView.getColumns().addAll(col_serviceId, col_name, col_price);
        //tableView.setTableMenuButtonVisible(true);
        tableView.setColumnResizePolicy(TableView.UNCONSTRAINED_RESIZE_POLICY);
        
    }
    
    private void editServices(ServicesRow row){
        Stage stage = new Stage();
        Parent root=null;
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("EditService.fxml"));     
            root = (Parent)fxmlLoader.load();  
            EditServiceController controller = fxmlLoader.<EditServiceController>getController();
            if(row!=null){
                controller.setName(row.getName());
                controller.setPrice(row.getPrice());
                controller.setID(row.getServiceID()+"");
            }
            JFXDecorator decorator = new JFXDecorator(stage, root);
            decorator.setCustomMaximize(false);
            Scene scene = new Scene(decorator);   
            scene.getStylesheets().add(LawnCareSystem.class.getResource("/resources/css/style.css").toExternalForm());
            stage.setScene(scene);
            stage.setResizable(false);
           // stage.setMaximized(true);
            stage.setOnHidden(new EventHandler<WindowEvent>() {
                    @Override
                    public void handle(WindowEvent event) {
                        if(controller.changesMade){
                            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Would you like to save the changes?",
                                 ButtonType.YES, 
                                 ButtonType.NO);
                            alert.setTitle("Confirm Changes");
                            alert.setHeaderText(null);
                            Optional<ButtonType> result = alert.showAndWait();
                            if (result.get() == ButtonType.YES) {
                                controller.saveChanges();
                                //refreshTable();
                            }
                        }
                        refreshSettings();
                    }
                });
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(CustomersController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void refreshSettings(){
        data.clear();
        populateDataset();
    }
    public void taxSetButtonClicked(){
        double taxPct = Double.parseDouble(taxPercent.getText());
        if(taxPct<=100 && taxPct > 0){
            MasterControl.access().setTaxPercent(taxPct);
        }
    }
    
    public void newServiceButtonClicked(){
        editServices(null);
    }
    
    public void deleteButtonClicked(){
        ObservableList l = tableView.getSelectionModel().getSelectedCells();
        ObservableList<ServicesRow> objects = tableView.getSelectionModel().getSelectedItems();
        
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirm Deletion");
        alert.setHeaderText(null);
        alert.setContentText("Are you sure you want to delete "+l.size()+" services from your database? "
                + "This action cannot be undone.");
        Optional <ButtonType> action = alert.showAndWait();
        
        if(action.get() == ButtonType.OK){
            for(int i=0; i<l.size();++i){
                int row = ((TablePosition) l.get(i) ).getRow();
                Integer id = (Integer)col_serviceId.getCellData(row);
                try {
                    Connection conn = DBConnection.getDBConnection();
                    conn.createStatement().executeUpdate("UPDATE Services SET Active = 0 WHERE Service_ID = "+id);
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(BillingController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            data.removeAll(objects);
            refreshSettings();
        }
        tableView.getSelectionModel().clearSelection();
       
    }
}
