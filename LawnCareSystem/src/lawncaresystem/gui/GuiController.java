/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lawncaresystem.gui;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDecorator;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXPopup;
import com.jfoenix.controls.JFXPopup.PopupHPosition;
import com.jfoenix.controls.JFXPopup.PopupVPosition;
import com.jfoenix.controls.JFXRippler;
import com.jfoenix.controls.JFXRippler.RipplerMask;
import com.jfoenix.controls.JFXRippler.RipplerPos;
import com.jfoenix.controls.JFXSnackbar;
import com.jfoenix.controls.JFXSnackbar.SnackbarEvent;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.transitions.hamburger.HamburgerBackArrowBasicTransition;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javafx.animation.Animation;
import javafx.animation.RotateTransition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import lawncaresystem.LawnCareSystem;
import lawncaresystem.Util.LogUtil;


/**
 *
 * @author Jonathon
 */
public class GuiController implements Initializable {
    
    @FXML
    private BorderPane root;
    @FXML
    private Button settingsButton;
    @FXML
    private ImageView settingsImageView;
    @FXML
    private JFXTabPane tabPane;
    @FXML
    private BorderPane contentPane;
    
    public static LogUtil Log = LogUtil.getLogger();
    private Stage settingsStage = new Stage();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        String imgPath = ( System.getProperty("os.name").toLowerCase().contains("win") )
                            ? System.getProperty("user.dir")+"\\src\\resources\\images\\settings.png"
                            : System.getProperty("user.dir")+"/src/resources/images/settings.png";
        File f = new File( imgPath );
        Image image = new Image(f.toURI().toString());
        ImageView iv = new ImageView(image);
        settingsButton.setGraphic(iv);
        
        setUpSettingsStage();
        settingsStage.getIcons().add(image);
        //settingsButton.setOnMouseClicked(e->rotation.play());
        settingsButton.setOnAction(e->{
            RotateTransition rotation = new RotateTransition(Duration.seconds(0.5), settingsButton);
            rotation.setCycleCount(1);
            rotation.setByAngle(180);
            rotation.setRate(.8);
            rotation.play();
            if(!settingsStage.isShowing())
                settingsStage.show();
        });
        // tab pane
        tabPane.getSelectionModel().clearSelection();

        // Add Tab ChangeListener
        tabPane.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tab>() {
            @Override
            public void changed(ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) {
                System.out.println("Tab selected: " + newValue.getText());
                if(newValue.getText()!=null){
                    contentPane.getChildren().clear();
                    try {
                        System.out.println(newValue.getText()+"/" +newValue.getText()+".fxml");
                        FXMLLoader loader = new FXMLLoader();
                        loader.setLocation(this.getClass().getResource("/lawncaresystem/gui/"+newValue.getText()+"/" +newValue.getText()+".fxml"));
                        contentPane.setCenter(loader.load());
                        //contentPane.getChildren().add(loader.load());
                        contentPane.getStylesheets().add(LawnCareSystem.class.getResource("/resources/css/"+newValue.getText()+".css").toExternalForm());
                    } catch (IOException ex) {
                        java.util.logging.Logger.getLogger(GuiController.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }else{
                    
                }
                if (newValue.getContent() == null) {
                    try {
                    

                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } else {

                }
            }
        });
        // By default, select 1st tab and load its content.
        tabPane.getSelectionModel().selectFirst();
    }
    
    @FXML
    private void setUpSettingsStage(){
        final FXMLLoader loader = new FXMLLoader(getClass().getResource("Settings.fxml"));
        try {
            final BorderPane
            r = FXMLLoader.load(getClass().getResource("Settings.fxml"));
            JFXDecorator decorator = new JFXDecorator(settingsStage, r);
            final Scene scene = new Scene(decorator, 800, 500);
            scene.getStylesheets().add(LawnCareSystem.class.getResource("/resources/css/style.css").toExternalForm());
            decorator.setCustomMaximize(true);
//            settingsStage.initOwner(settingsButton.getScene().getWindow());
            settingsStage.setTitle("Settings");
            settingsStage.setScene(scene);

            
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(GuiController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
}
