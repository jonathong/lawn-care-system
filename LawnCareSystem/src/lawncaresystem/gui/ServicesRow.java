/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lawncaresystem.gui;

import lawncaresystem.gui.Customers.*;
import lawncaresystem.gui.Billing.*;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Jonathon
 * 
 */

public class ServicesRow {
    private final SimpleIntegerProperty serviceID;
    private final SimpleStringProperty name;
    private final SimpleStringProperty price;   
    
    public ServicesRow(String id, String n, String p){
        
        this.serviceID = new SimpleIntegerProperty(Integer.parseInt(id));
        this.name = new SimpleStringProperty(n);
        this.price = new SimpleStringProperty(p);

    }

    public Integer getServiceID() {
        return serviceID.get();
    }

    public String getName() {
        return name.get();
    }

    public String getPrice() {
        return price.get();
    }

    public void setServiceID(String id){
        serviceID.set(Integer.parseInt(id));
    }
    
    public void setName(String n){
        name.set(n);
    }
    
    public void setPrice(String p){
        price.set(p);
    }
    
}
